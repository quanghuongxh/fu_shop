<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code')->unique();
            $table->string('description');
            $table->text('content');
            $table->integer('category_id')->unsigned()->default(0);
            $table->integer('brand_id')->unsigned()->nullable()->default(0);
            $table->integer('stock_id')->unsigned()->nullable()->default(0);
            $table->string('images')->nullable();
            $table->integer('quantity')->default(0);
            $table->decimal('import_price',10,  0)->nullable();
            $table->decimal('sell_price', 10, 0)->default(0);
            $table->decimal('sale_price', 10, 0)->nullable()->default(0);
            $table->integer('num_view')->default(1);
            $table->integer('num_sell')->default(0);
            $table->boolean('is_featured')->default(0);
            $table->boolean('available')->default(1);
            $table->string('color', 100)->nullable();
            $table->string('size', 50)->nullable();
            $table->string('material', 50)->nullable();
            $table->string('made_in', 100)->nullable();
            $table->double('rate', 15, 8)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

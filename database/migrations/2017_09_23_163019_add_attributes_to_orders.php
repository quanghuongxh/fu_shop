<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributesToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('orders', function($table) {
		    $table->text('cart');
		    $table->string('name');
		    $table->string('payment_id')->nullable();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('orders', function($table) {
		    $table->dropColumn('cart');
		    $table->dropColumn('name');
		    $table->dropColumn('payment_id');
	    });
    }
}

<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,6) as $index) {
            DB::table('brands')->insert([
                'name' => $faker->name,
                'available' => rand(0,1),
                'image' => '',
            ]);
        }
    }
}

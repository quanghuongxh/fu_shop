<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create default category
        DB::table('categories')->insert([
            'id' => 1,
            'name' => 'Uncategory',
            'description' => 'Not category',
            'parent_id' => 0,
        ]);

        //create seeder for category
        $faker = Faker::create();
        foreach (range(1,6) as $index) {
            DB::table('categories')->insert([
                'name' => $faker->name,
                'description' => $faker->text(200),
                'parent_id' => 0,
                
            ]);
        }

    }
}

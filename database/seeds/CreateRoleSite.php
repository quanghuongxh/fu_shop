<?php

use Illuminate\Database\Seeder;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class CreateRoleSite extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//create customer role
        Sentinel::getRoleRepository()->createModel()->create([
		    'name' => 'Customer',
		    'slug' => 'customer',
		]);
		Sentinel::getRoleRepository()->createModel()->create([
		    'name' => 'Admin',
		    'slug' => 'admin',
		]);

		Sentinel::getRoleRepository()->createModel()->create([
		    'name' => 'Manager',
		    'slug' => 'manager',
		]);

		//asign user
		$user = Sentinel::findById(1);
		$role = Sentinel::findRoleByName('Admin');
		$role->users()->attach($user);
    }
}

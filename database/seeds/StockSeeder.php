<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class StockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create default manafacture
        DB::table('stocks')->insert([
            'id' => 1,
            'name' => 'Dont manager stock',
            'address' => '',
        ]);
        //
        $faker = Faker::create();
        foreach (range(1,6) as $index) {
            DB::table('stocks')->insert([
                'name' => $faker->name,
                'address' => $faker->address,
            ]);
        }
    }
    
}

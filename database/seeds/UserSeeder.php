<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        //
        //create default admin
        //create default category
        DB::table('users')->insert([
                'email' => 'admin@fushop.dev',
                'password' => bcrypt('123456'),
                'last_login' => $faker->dateTime,
                'first_name' => 'Admin',
                'last_name' => 'Fushop',
                'avatar' => $faker->imageUrl(100,100),
                'phone' => $faker->phoneNumber,
                'address' => $faker->address,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
        ]);
        $user = Sentinel::findById(1);
        $admin_role = Sentinel::findRoleBySlug('admin');
        $admin_role->users()->attach($user);
    
        foreach (range(1,10) as $index) {        
            $user = DB::table('users')->insert([
                'email' => $faker->email,
                'password' => bcrypt('123456'),
                'last_login' => $faker->dateTime,
                'first_name' => $faker->firstName(),
                'last_name' => $faker->lastName,
                'avatar' => $faker->imageUrl(100,100),
                'phone' => $faker->phoneNumber,
                'address' => $faker->address,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }

    }
}

<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create();
        foreach (range(1,10) as $index) {
            DB::table('products')->insert([
                'name' => $faker->name,
                'code' => $faker->ean8,
                'description' => $faker->text(100),
                'content' => $faker->text,
                'category_id' => rand(1,6),
                'brand_id' => rand(1,6 ),
                'stock_id' => rand(1,6 ),
                'quantity' => rand(1,200),
                'import_price' => rand (1, 500),
                'sell_price' => rand (1, 500),
                'num_view' => rand(1, 100),
                'num_sell' => rand(1, 100),
                'is_featured' => rand(0,1),
                'color' => $faker->colorName,
                'size' => '22x30',  
                'material' => 'Wood',
                'made_in' => 'VietNam'
            ]);
        }
    }
}

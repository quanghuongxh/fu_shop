@extends('shop.layout.app')

@section('content')
<div class="container">
    <div class="padding-top"></div>
    <div class="row" style="padding: 3em 0;">
        <div class="col-sm-6 col-sm-offset-3">
            <h4>LOGIN WITH YOUR EMAIL ADDRESS</h4>
            <form action="{{ route('customer.login') }}" method="post">
                 @if(session('err'))
                    <div class="alert alert-warning" role="alert"> { { session('err')}}</div>
                @endif
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="">Email</label>
                    {{--<input type="text" class="form-control" id="" placeholder="">--}}
                    <input type="email" name="email" class="form-control" placeholder="Email" style="width: 60%">
                    <span class="help-block">{{ $errors->first('emails') }}</span>
                </div>
                <div class="form-group">
                    {{--<label for="">Password</label>--}}
                    {{--<input type="password" class="form-control" id="" placeholder="">--}}
                    <input type="password" name="password" class="form-control" placeholder="Password" style="width: 60%">
                        <span class="help-block">{{ $errors->first('password') }}</span>
                </div>
                <button type="submit" class="btn btn-primary">Login</button>
                {{-- <span class="psw">Forgot <a href="#">Have you forgotten your password?</a></span> --}}
                <span>You don't have an account?</span><a href="{{ route('customer.register') }}"> SIGN UP</a>
            </form>
        </div>
    </div>
</div>
@endsection


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Đăng ký tài khoản</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('assets') }}/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('assets') }}/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('assets') }}/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
   <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/AdminLTE.min.css">

    <link rel="stylesheet" href="{{ asset('assets') }}/plugins/iCheck/square/blue.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="#">Funiture Shop</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Đăng kí</p>

    <form action="{{ route('user.register') }}" method="POST">
      {{ csrf_field() }}
      <div class="form-group has-feedback @if( $errors->has('first_name') ) has-error @endif">
        <input name="first_name" type="text" class="form-control" placeholder="Họ" value="{{ old('first_name') }}">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('first_name') }}</span>
      </div>

      <div class="form-group has-feedback @if( $errors->has('last_name') ) has-error @endif">
        <input name="last_name" type="text" class="form-control" placeholder="Tên" value="{{ old('last_name') }}">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('last_name') }}</span>
      </div>

      <div class="form-group has-feedback @if( $errors->has('emails') ) has-error @endif">
        <input name="email" type="email" class="form-control" placeholder="Email" value="{{ old('email') }}">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('email') }}</span>
      </div>

      <div class="form-group has-feedback @if( $errors->has('password') ) has-error @endif">
        <input name="password" type="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('password') }}</span>
      </div>

      <div class="form-group has-feedback @if( $errors->has('password_confirmation') ) has-error @endif">
        <input name="password_confirmation" type="password" class="form-control" placeholder="Retype password">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
      </div>

      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input name="check-term" type="checkbox"> Đồng ý điều khoản của shop
              <span class="help-block" style="color:pink">{{ $errors->first('check-term') }}</span>
            </label>
          </div>
        </div>

        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Đăng ký</button>
        </div>
        <!-- /.col -->
      </div>
    </form>


    <a href="{{ route('user.login') }}" class="text-center">Đả có tài khoản? Đăng nhập ngay!</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="{{ asset('assets') }}/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('assets') }}/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="{{ asset('assets') }}/plugins/iCheck/icheck.min.js"></script>
<script src="{{ asset('assets/plugins/jquery-validation') }}/jquery.validate.min.js"></script>
<script src="{{ asset('assets/plugins/jquery-validation') }}/localization/messages_vi.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });


</script>
</body>
</html>

@extends('shop.layout.app')
@section('style')
@stop 
@section('content')
<div class="container" style="background-color: #ffff">
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4">
            <h3 align="left" style="padding-bottom: 1em">Create new customer account</h3>
            <form action="{{ route('customer.register') }}" method="POST" style="padding-bottom: 1em">
                {{ csrf_field() }}

                <div class="form-group has-feedback @if( $errors->has('first_name') ) has-error @endif">
                    <input name="first_name" type="text" class="form-control" style="width: 100%" placeholder="First name" value="{{ old('first_name') }}" >
                    <span class="help-block">{{ $errors->first('first_name') }}</span>
                </div>

                <div class="form-group has-feedback @if( $errors->has('last_name') ) has-error @endif">
                    <input name="last_name" type="text" class="form-control" style="width: 100%" placeholder="Last name" value="{{ old('last_name') }}">
                    <span class="help-block">{{ $errors->first('last_name') }}</span>
                </div>

                <div class="form-group has-feedback @if( $errors->has('email') ) has-error @endif">
                    <input name="email" type="email" class="form-control" style="width: 100%" placeholder="Email" value="{{ old('email') }}">
                    <span class="help-block">{{ $errors->first('email') }}</span>
                </div>

                <div class="form-group has-feedback @if( $errors->has('password') ) has-error @endif">
                    <input name="password" type="password" class="form-control" style="width: 100%" placeholder="Password">
                    <span class="help-block">{{ $errors->first('password') }}</span>
                </div>

                <div class="form-group has-feedback @if( $errors->has('password_confirmation') ) has-error @endif">
                    <input name="password_confirmation" type="password" class="form-control" style="width: 100%" placeholder="Retype password">
                    <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                </div>

                <div class="row">
                    <div class="col-sm-8">
                        <div class="checkbox icheck">
                            <label>
                                <input name="check-term" type="checkbox"> Accept the terms of the Fushop
                                <span class="help-block" style="color:pink">{{ $errors->first('check-term') }}</span>
                            </label>
                        </div>
                    </div>

                    <!-- /.col -->
                    <div class="col-sm-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">SUBMIT</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
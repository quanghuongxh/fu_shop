@extends('admin.layouts.master')

<!--------------- page title --------------- -->
@section('title')
    Tạo khách hàng
@endsection

<!--------------- start content ----------------->
@section('content')

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12 col-md-12">
            <div class="box">
                <form role="form" id="customer-form" style="padding:20px" method="post" action="{{ route('customer.store') }}">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="row"> <!-- quan ly thong tin co ban -->
                            <div class="col-lg-3 col-md-3">
                                <div class="help-block">
                                    <h4>Thông tin cơ bản</h4>
                                    <p style="color:#cecece;">Cung cấp thông tin của khách hàng.</p>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group @if( $errors->has('emails') ) has-error @endif">
                                            <label class="control-label" for=""> Email</label>
                                            <input  value="{{ old('email') }}" type="text" class="form-control" name="email" id="inputEmail" placeholder="Email">
                                            <span class="help-block">{{ $errors->first('emails') }}</span>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group @if( $errors->has('password') ) has-error @endif">
                                            <label class="control-label" for=""> Mật khẩu</label>
                                            <input  value="" type="text" class="form-control" name="password" id="inputPassword" placeholder="Password">
                                            <span class="help-block">{{ $errors->first('password') }}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group @if( $errors->has('first_name') ) has-error @endif">
                                            <label class="control-label" for=""> Họ </label>
                                            <input value="{{ old('first_name') }}" type="text" class="form-control" name="first_name" id="inputFirstname" placeholder="Họ">
                                            <span class="help-block">{{ $errors->first('first_name') }}</span>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group @if( $errors->has('last_name') ) has-error @endif">
                                            <label class="control-label" for=""> Tên </label>
                                            <input value="{{ old('last_name') }}" type="text" class="form-control" name="last_name" id="inputLastname" placeholder="Tên">
                                            <span class="help-block">{{ $errors->first('last_name') }}</span>
                                        </div>
                                    </div>
                                </div>

                                <hr />
                            </div>
                        </div> <!-- end: quan ly thong tin -->

                        <div class="row">

                            <div class="col-lg-3 col-md-3">
                                <div class="help-block">
                                    <h4>Thông tin liên lạc</h4>
                                    <p style="color:#cecece;"> Các thông tin liên lạc</p>
                                </div>
                            </div>

                            <div class="col-lg-9 col-md-9">
                                <div class="row" style="margin-top: 20px">

                                    <div class="col-md-12 col-lg-12">
                                        <div class="form-group @if( $errors->has('phone') ) has-error @endif">
                                            <label class="control-label" for=""> Điện thoại</label>
                                            <input  value="{{ old('phone') }}" type="text" class="form-control" name="phone" id="inputPhone" placeholder="Số điện thoại">
                                            <span class="help-block">{{ $errors->first('phone') }}</span>
                                        </div>

                                         <div class="form-group @if( $errors->has('company') ) has-error @endif">
                                            <label class="control-label" for=""> Nơi làm viẽc</label>
                                            <input value="{{ old('company') }}" type="text" class="form-control" name="company" id="inputCompany" placeholder="Công ty">
                                            <span class="help-block">{{ $errors->first('company') }}</span>
                                        </div>

                                         <div class="form-group @if( $errors->has('address') ) has-error @endif">
                                            <label class="control-label" for=""> Địa chỉ</label>
                                            <input  value="{{ old('address') }}" type="text" class="form-control" name="address" id="inputAddress" placeholder="Địa chỉ">
                                            <span class="help-block">{{ $errors->first('address') }}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 col-lg-12">
                                       <div class="form-group  @if( $errors->has('note') ) has-error @endif">
                                            <label class="control-label" for=""> Ghi chú </label>
                                            <textarea id="ckeditor" class="ckeditor" name="note">{{ old('note') }}</textarea>
                                            <span class="help-block">{{ $errors->first('note') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- /quan ly kho -->
                      
                        <hr />

                        <div class="row">
                            <div class="col-lg-3 col-md-3">

                            </div>
                            <div class="col-lg-9 col-md-9" style="padding-top: 20px;">

                                <div class="btn-group pull-right">
                                    <button type="submit" type="button" class="btn btn-info">Lưu</button>
                                </div>
                                <div class="btn-group pull-left">
                                    <a href="{{ route('customer.list') }}" class="btn btn-default">Trở lại</a>

                                </div>
                            </div>

                        </div>

                    </div><!--/form-body -->
                </form>

            </div>
        </div>
    </div>

@endsection


<!--------------- custom page css -------------->
@section('page-css')
    <style type="text/css">
        .error {
            color:red;
            font-weight: normal;
        }
    </style>

@endsection

<!--------------- custom header javascript-------->
@section('page-header-js')

@endsection

<!--------------- custom footer javascript-------->
@section('page-footer-js')

    <!-- jquery editor and finder -->
    <script src="{{ asset('assets/plugins/editor/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/plugins/editor/ckfinder/ckfinder.js') }}"></script>
   
    <!-- jquery validation -->
    <script src="{{ asset('assets/plugins/jquery-validation') }}/jquery.validate.min.js"></script>
    <script src="{{ asset('assets/plugins/jquery-validation') }}/localization/messages_vi.min.js"></script>
    <script>
        $("#customer-form").validate({
            rules:{
                email: {
                    required:true,
                    email:true,
                },
                first_name: {
                    required:true,
                },
                last_name: {
                    required:true,
                },
                password: {
                    required:true,
                    minlength:3
                },
                phone: {
                    required:true,
                    digits:true,
                },
                address: {
                    required:true,
                },

            }
        });
    </script>
    <!-- jquery fancy box -->

@endsection
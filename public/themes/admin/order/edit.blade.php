@extends('admin.layouts.master')

<!--------------- page title --------------- -->
@section('title')
    Order detail
@endsection

<!--------------- start content ----------------->
@section('content')

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12 col-md-12">
            <div class="box">

                    <!-- start imput here -->
                    <div class="row">
                        <!-- for create order -->
                        <div class="col-lg-2 col-md-2">
                                <div class="help-block">
                                    <h4>Basic infomation</h4>
                                    <p style="color:#cecece;">Infomation of order</p>
                                </div>
                        </div>

                        <div class="col-md-5">
                            <form role="form" id="order-form" style="padding:10px" method="POST" action="{{ route('order.change-status') }}">
                                {{ csrf_field() }}

                                
                                <input type="hidden" name="id" value="{{ $order->id }}" />

                                <p class="text-info">Order id: <span>#{{ $order->id }} </span></p>
                                <p class="text-info">Customer name: <span>{{ $order->customer->first_name }}  {{ $order->customer->last_name }} </span></p>
                                <p class="text-info">Email: <span> {{ $order->emails }} </span></p>
                                <p class="text-info">Address: <span>{{ $order->address }} </span></p>
                                <p class="text-info">Phone: <span>{{ $order->phone }} </span></p>
                                <p class="text-info">Email: <span>{{ $order->emails }} </span></p>
                                <label class="text-warning">Order Status
                                <select name="status" class="form-control">
                                    <option value = "0" @if ($order->status == 0) selected @endif> {{ get_order_status(0) }} </option>
                                    <option value = "1" @if ($order->status == 1) selected @endif> {{ get_order_status(1) }} </option>
                                    <option value = "2" @if ($order->status == 2) selected @endif> {{ get_order_status(2) }} </option>
                                    <option value = "3" @if ($order->status == 3) selected @endif> {{ get_order_status(3) }} </option>
                                </select>
                                </label>
                                <hr />
                                


                                <div class="btn-group pull-left">
                                    <button type="submit" class="btn btn-info">Save</button>
                                </div>
                                <div class="clearfix"></div>

                            </form>


                        </div>
                        <div class="col-md-5">
                           <table class="table table-hoverd">
                                <thead>
                                    <th>Image</th>
                                    <th>Product</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Total</th>
                                </thead>
                                <tbody>
                            
                                @foreach ($order->products as $product)
                                    <tr>
                                        <td><img src="{{ url('').'/' . $product->featured_img() }}" width="50" height="70" /></td>
                                        <td> {{ $product->name }} </td>
                                        <td> {{ $product->pivot->quantity }} </td>
                                        <td> {{ $product->sell_price }} </td>
                                        <?php $price = $product->sell_price * $product->pivot->quantity ?>
                                        <td> {{ $price }} </td>
                                    </tr>
                                @endforeach
                                </tbody>
                           </table>
                           <hr />
                           Total Order: <strong>{{ $order->total_price }}</strong>
                        </div>

                    </div>

            </div>
        </div>
    </div>

@endsection


<!--------------- custom page css -------------->
@section('page-css')
    <style type="text/css">
        #order-form p{
            
            color: #000;
        }
        #order-form p span{
            color:blue;
            padding-left:50px;
        }
    </style>

@endsection

<!--------------- custom header javascript-------->
@section('page-header-js')

@endsection

<!--------------- custom footer javascript-------->
@section('page-footer-js')
   

@endsection
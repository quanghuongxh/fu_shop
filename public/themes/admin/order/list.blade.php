@extends('admin.layouts.master')

@section('content')
    <div class="row">
        <div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Data Table With Full Features</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Customer</th>
                     <th>Email</th>
                    <th>Create at</th>
                    <th>Status</th>
                  
                    <th>#</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($orders as $order)
                
                <tr>
                    <td>#{{ $order->id }}</td>
                    <td>
                        {{ $order->customer->first_name }}   {{ $order->customer->first_name }}
                    </td>
                    <td>{{ $order->customer->email }}</td>
                    <td>{{ $order->created_at }}</td>
                    <td> 
                        {{ get_order_status($order->status) }}
                    </td>
                    <td>
                         <a href="{{ route('order.view', $order->id) }}" class="btn btn-xs btn-info"><i class="fa  fa-edit"></i> </a>
                        <form action="{{ route('order.delete', $order->id) }}" method="POST" onsubmit="return confirm_delete();">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="delete-btn btn btn-xs btn-warning"><i class="fa fa-remove"></i> </button>
                        </form>
                    </td>
                </tr>
                @endforeach
                </tbody>

                <tfoot>
                <tr>
                    <th>#</th>
                    <th>Customer</th>
                    <th>Email</th>
                    <th>Create at</th>
                    <th>Status</th>
                    <th>#</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
        </div>
    </div>


@endsection

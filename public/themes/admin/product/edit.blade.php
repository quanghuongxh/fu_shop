@extends('admin.layouts.master')

<!--------------- page title --------------- -->
@section('title')
    Sửa sản phẩm
@endsection

<!--------------- start content ----------------->
@section('content')

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12 col-md-12">
            <div class="box">
                <form role="form" id="product-form" style="padding:20px" method="post" action="{{ route('product.edit') }}">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{ $product->id }}" name="id">
                    <div class="box-body">
                        <div class="row"> <!-- quan ly thong tin co ban -->
                            <div class="col-lg-3 col-md-3">
                                <div class="help-block">
                                    <h4>Thông tin cơ bản</h4>
                                    <p style="color:#cecece;">Cung cấp thông tin về tên, mô tả loại sản phẩm và nhà sản xuất để phân loại sản phẩm này.</p>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group @if( $errors->has('name') ) has-error @endif">
                                            <label class="control-label" for=""> Tên sản phẩm </label>
                                            <input required value="{{ $product->name }}" type="text" class="form-control" name="name" id="inputName" placeholder="Name">
                                            <span class="help-block">{{ $errors->first('name') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group @if( $errors->has('code') ) has-error @endif">
                                            <label class="control-label" for=""> Mã sản phẩm </label>
                                            <input value="{{ $product->code or '' }}" type="text" class="form-control" name="code" id="inputCode" placeholder="Product code">
                                            <span class="help-block">{{ $errors->first('code') }}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group  @if( $errors->has('description') ) has-error @endif">
                                    <label class="control-label" for="inputShort"> Mô tả ngắn </label>
                                    <textarea id="inputShort" name="description" class="form-control">{{ $product->description or '' }}</textarea>
                                    <span class="help-block">{{ $errors->first('description') }}</span>
                                </div>

                                <div class="form-group  @if( $errors->has('content') ) has-error @endif">
                                    <label class="control-label" for="inputDesc"> Chi tiết sản phẩm </label>
                                    <textarea id="ckeditor" class="ckeditor" name="content">{{ $product->content }}</textarea>
                                    <span class="help-block">{{ $errors->first('content') }}</span>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label>Danh mục</label>
                                            <select class="form-control" name="category_id">
                                                @foreach ($list_category as $cat)
                                                    @if (isset($product->category->id) && $cat->id == $product->category->id )
                                                        <option selected value="{{ $cat->id }}">{{ $cat->name }}</option>
                                                    @endif
                                                    <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label>Hãng sản xuất</label>
                                            <select class="form-control" name="brand_id">
                                                @foreach ($list_brand as $brand)
                                                    @if ( isset($product->brand->id) && $brand->id == $product->brand->id )
                                                        <option selected value="{{ $brand->id }}">{{ $brand->name }}</option>
                                                    @endif
                                                    <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group @if( $errors->has('color') ) has-error @endif">
                                                <label class="control-label" for=""> Color </label>
                                                <input  value="{{ $product->color }}" type="text" class="form-control" name="color" id="inputColor" placeholder="Color">
                                                <span class="help-block">{{ $errors->first('color') }}</span>
                                        </div>
                                         <div class="form-group @if( $errors->has('size') ) has-error @endif">
                                                <label class="control-label" for=""> Size: </label>
                                                <input  value="{{ $product->size }}" type="text" class="form-control" name="size" id="inputSize" placeholder="Size">
                                                <span class="help-block">{{ $errors->first('size') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                         <div class="form-group @if( $errors->has('material') ) has-error @endif">
                                                <label class="control-label" for=""> Material: </label>
                                                <input  value="{{ $product->material }}" type="text" class="form-control" name="material"  placeholder="Material">
                                                <span class="help-block">{{ $errors->first('material') }}</span>
                                        </div>
                                         <div class="form-group @if( $errors->has('made_in') ) has-error @endif">
                                                <label class="control-label" for=""> Made in: </label>
                                                <input  value="{{ $product->made_in }}" type="text" class="form-control" name="made_in" placeholder="Made from">
                                                <span class="help-block">{{ $errors->first('made_in') }}</span>
                                        </div>
                                    </div>
                                    
                                </div>
                                <hr />
                            </div>
                        </div> <!-- end: quan ly thong tin -->


                        <div class="row"><!-- quanr ly hinh anh -->
                            <div class="col-lg-3 col-md-3">
                                <div class="help-block">
                                    <h4>Upload hình ảnh sản phẩm</h4>
                                    <p style="color:#cecece;">Hình mô ta minh họa sản phẩm <br/> kích thước chuẩn < 2mb.</p>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9">
                                <div class="row upload-area" >
                                    <div class="col-lg-10 col-md-10">
                                        <input type="hidden" value="{{ $product->images }}" name="images" id="image-url"/>
                                        <div class="img-show">
                                           @php
                                               $product_image_list = $product->get_product_images();
                                           @endphp
                                            @if ( is_array( $product_image_list ) )

                                                @foreach ($product_image_list as $image)
                                                    <img class="product-img"  src="{{URL::asset($image) }}" width="150" height="150" />
                                                @endforeach

                                            @endif

                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 ">
                                        <a href="javascript:void(0);" class="btn btn-info pull-right " id="ckfinder-btn">Thêm ảnh</a>
                                    </div>
                                </div>
                                <hr />
                            </div>
                        </div>


                        <div class="row"> <!-- quan ly kho -->
                            <div class="col-lg-3 col-md-3">
                                <div class="help-block">
                                    <h4>Thông tin kho hàng</h4>
                                    <p style="color:#cecece;"> Cung cấp thông tin về nhập xuất sản phẩm như: giá, nơi chứa sản phẩm.</p>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9">
                                <div class="row" style="margin-top: 20px">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group  @if( $errors->has('import_price') ) has-error @endif">
                                            <label class="control-label" for=""> Giá nhập vào </label>
                                            <input  value="{{ $product->import_price or '' }}" type="text" class="form-control number" name="import_price" id="import_price" placeholder="Giá nhập vào">
                                            <span class="help-block">{{ $errors->first('import_price') }}</span>
                                        </div>
                                        <div class="form-group  @if( $errors->has('sell_price') ) has-error @endif">
                                            <label class="control-label" for=""> Giá bán </label>
                                            <input required   value="{{ $product->sell_price }}" type="text" class="form-control number" name="sell_price" id="sell_price placeholder="Giá bán">
                                            <span class="help-block">{{ $errors->first('sell_price') }}</span>
                                        </div>
                                        <div class="form-group  @if( $errors->has('sale_price') ) has-error @endif">
                                            <label class="control-label" for=""> Giá sale </label>
                                            <input  value="{{ $product->sale_price or '' }}" type="text" class="form-control number" name="sale_price" id="sale_price" placeholder="Giá sale">
                                            <span class="help-block">{{ $errors->first('sale_price') }}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group  @if( $errors->has('quantity') ) has-error @endif">
                                            <label class="control-label" for=""> Số lượng </label>
                                            <input value="{{ $product->quantity }}"  type="text" class="form-control number" name="quantity" id="quantity" placeholder="Số lượng">
                                            <span class="help-block">{{ $errors->first('quantity') }}</span>
                                        </div>
                                        <div class="form-group">
                                            <label>Kho</label>
                                            <select class="form-control" name="stock_id">
                                                <option value="0" selected>Không lưu kho</option>
                                                @foreach ($list_stock as $stock)
                                                    @if (isset($product->stock) && $stock->id == $product->stock->id )
                                                        <option selected value="{{ $stock->id }}">{{ $stock->name }}</option>
                                                    @endif
                                                    <option value="{{ $stock->id }}">{{ $stock->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div> <!-- /quan ly kho -->


                        <div class="row">
                            <div class="col-lg-3 col-md-3">

                            </div>
                            <div class="col-lg-9 col-md-9">
                                <div class="form-group">
                                    <label class="checkbox-inline"><input @if ($product->is_featured) checked  @endif name="is_featured" type="checkbox">Sản phẩm nổi bật</label>
                                    <label class="checkbox-inline"><input @if ($product->available) checked  @endif name="available" type="checkbox">Hiển thị</label>
                                </div>
                            </div>
                        </div>

                        <hr />

                        <div class="row">
                            <div class="col-lg-3 col-md-3">

                            </div>
                            <div class="col-lg-9 col-md-9" style="padding-top: 20px;">

                                <div class="btn-group pull-right">
                                    <button type="submit" type="button" class="btn btn-info">Lưu</button>
                                </div>
                                <div class="btn-group pull-left">
                                    <a href="{{ route('product.list') }}" class="btn btn-default">Trở lại</a>

                                </div>
                            </div>

                        </div>

                    </div><!--/form-body -->
                </form>

            </div>
        </div>
    </div>

@endsection


<!--------------- custom page css -------------->
@section('page-css')
    <style type="text/css">
        .error {
            color:red;
            font-weight: normal;
        }
    </style>

@endsection

<!--------------- custom header javascript-------->
@section('page-header-js')

@endsection

<!--------------- custom footer javascript-------->
@section('page-footer-js')

    <!-- jquery editor and finder -->
    <script src="{{ asset('assets/plugins/editor/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/plugins/editor/ckfinder/ckfinder.js') }}"></script>
    <script type="text/javascript">
        var button1 = document.getElementById( 'ckfinder-btn' );
        var image_array = [];
        var old_images = document.getElementById( 'image-url' ).value;
        console.log (old_images);
        if (old_images.length > 0) {
            old_images = JSON.parse(old_images);
        }

        for (var i = 0; i < old_images.length; i++) {
            image_array.push(old_images[i]);
        }

        console.log( old_images.length );


        button1.onclick = function() {
            selectFileWithCKFinder( 'image-url' );
        };
        function selectFileWithCKFinder( elementId ) {
            CKFinder.modal( {
                chooseFiles: true,
                width: 800,
                height: 600,
                onInit: function( finder ) {
                    finder.on( 'files:choose', function( evt ) {
                        var file = evt.data.files.first();
                        var output = document.getElementById( elementId );
                        //add image url to array and save to hidden input
                        image_array.push(file.getUrl());
                        output.value = JSON.stringify(image_array);
                        //show images
                        $('.img-show').append('<img class="product-img"  src="{{ asset('') }}'+ file.getUrl() +'" width="150" height="150" />');
                    } );
                    finder.on( 'file:choose:resizedImage', function( evt ) {
                        var output = document.getElementById( elementId );

                        image_array.push(evt.data.resizedUrl);
                        output.value = JSON.stringify(image_array);
                        //show images
                        $('.img-show').append('<img class="product-img"  src="{{ asset('uploads') }}/images/product_default.jpg" width="150" height="150" />');
                    } );
                }
            } );
        }
    </script>

    <!-- jquery validation -->
    <script src="{{ asset('assets/plugins/jquery-validation') }}/jquery.validate.min.js"></script>
    <script src="{{ asset('assets/plugins/jquery-validation') }}/localization/messages_vi.min.js"></script>
    <script>
        $("#product-form").validate({
            rules:{
                import_price: {
                    number: true
                },
                sell_price: {
                    number: true,
                    required: true
                },
                sale_price: {
                    number: true,
                },
                quantity:{
                    number: true
                }

            }
        });
    </script>
    <!-- jquery fancy box -->

@endsection
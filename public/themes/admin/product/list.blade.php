@extends('admin.layouts.master')

<!--------------- page title --------------- -->
@section('title')
    Quản lý sản phẩm
@endsection

<!--------------- start content ----------------->
@section('content')

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Danh sách sản phẩm</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">

                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">

                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                <a href="{{ route('product.create') }}" class="btn btn-info">Tạo mới</a>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>ID</th>
                            <th>Sản phẩm</th>
                            <th>Hình ảnh</th>
                            <th>Danh mục</th>
                            <th>Trạng thái</th>
                            <th>Thao tác</th>
                        </tr>
                        @if ( count($list_product) >  0)

                        @foreach ($list_product as $product )
                        <tr>
                            <td> {{ $product->id }}</td>
                            <td> <a href="{{ route('product.view', $product->id) }}"> {{ $product->name }} </a> </td>
                            <td><img class="lazy" src="{{ url('').'/' . $product->featured_img() }}" width="40" height="60"/> </td>
                            <td> {{ $product->category->name or 'n/a' }} </td>

                            <td>
                                @if ($product->available == 1)
                                <span class="label label-success">
                                    available
                                </span>
                                @else
                                    <span class="label label-fail">
                                    disable
                                    </span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('product.view', $product->id) }}" class="btn btn-xs btn-info"><i class="fa  fa-edit"></i> </a>
                                <form action="{{ route('product.delete', $product->id) }}" method="POST" onsubmit="return confirm_delete();">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="delete-btn btn btn-xs btn-warning"><i class="fa fa-remove"></i> </button>
                                </form>

                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </table>
                </div>
                <!-- /.box-body -->
                {{ $list_product->links() }}
            </div>
            <!-- /.box -->
        </div>
    </div>

@endsection


<!--------------- custom page css -------------->
@section('page-css')

@endsection

<!--------------- custom header javascript-------->
@section('page-header-js')
    <script>
        function confirm_delete() {
            var confirm_delete = confirm('Xóa sản phẩm');
            return confirm_delete;
        }
    </script>
@endsection

<!--------------- custom footer javascript-------->
@section('page-footer-js')
    <script src="{{ asset('assets/plugins') }}/jquery.lazy/jquery.lazy.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(function() {
                $('.lazy').Lazy({
                    effect: 'fadeIn',
                    visibleOnly: true
                });

            });
        });

        //delete modal
    </script>

@endsection
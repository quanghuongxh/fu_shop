@extends('admin.layouts.master')

<!--------------- page title --------------- -->
@section('title')
    Branch Management
@endsection

<!--------------- start content ----------------->
@section('content')

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Branch</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">

                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">

                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                <a href="{{ route('brand.create') }}" class="btn btn-info">Create</a>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Photo</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        @if ( count($list_brand) >  0)

                            @foreach ($list_brand as $brand )
                                <tr>
                                    <td> {{ $brand->id }}</td>
                                    <td> <a href="{{ route('brand.view', $brand->id) }}"> {{ $brand->name }} </a> </td>
                                    <td>
                                        <img width="50" height="70" src="{{ asset('/') }}{{ $brand->image }}" alt="{{ $brand->name }}">
                                    </td>
                                  
                                    <td>
                                        @if ($brand->available)
                                            <span class="label label-success">
                                                <i class="fa fa-check"></i>
                                            </span>
                                        @else
                                            <span class="label label-fail">
                                                <i class="fa fa-close"></i>
                                            </span>
                                        @endif
                                    </td>
                                   
                                    <td>
                                       
                                        <a href="{{ route('brand.view', $brand->id) }}" class="btn btn-xs btn-info"><i class="fa  fa-edit"></i> </a>
                                        <form action="{{ route('brand.delete', $brand->id) }}" method="POST" onsubmit="return confirm_delete();">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="delete-btn btn btn-xs btn-warning"><i class="fa fa-remove"></i> </button>
                                        </form>
                                      
                                    </td>

                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
                <!-- /.box-body -->
                {{ $list_brand->links() }}
            </div>
            <!-- /.box -->
        </div>
    </div>

@endsection


<!--------------- custom page css -------------->
@section('page-css')

@endsection

<!--------------- custom header javascript-------->
@section('page-header-js')
    <script>
        function confirm_delete() {
            var confirm_delete = confirm('Xóa chuyên mục');
            return confirm_delete;
        }
    </script>
@endsection

<!--------------- custom footer javascript-------->
@section('page-footer-js')
   

@endsection
@extends('admin.layouts.master')

<!--------------- page title --------------- -->
@section('title')
    Edit Brand
@endsection

<!--------------- start content ----------------->
@section('content')

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12 col-md-12">
            <div class="box">

                    <!-- start imput here -->
                    <div class="row">
                        <!-- for create brand -->
                        <div class="col-lg-3 col-md-3">
                                <div class="help-block">
                                    <h4>Thông tin cơ bản</h4>
                                    <p style="color:#cecece;">Infomation of brand</p>
                                </div>
                        </div>

                        <div class="col-md-9">
                            <form role="form" id="brand-form" style="padding:10px" method="POST" action="{{ route('brand.edit') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $brand->id }}" />

                                <div class="form-group @if( $errors->has('name') ) has-error @endif">
                                    <label for="inputName">Name</label>
                                    <input id="name" name="name"  type="text" value="{{ $brand->name }}" class="form-control" placeholder="Tên" />
                                    <span class="help-block">{{ $errors->first('name') }}</span>
                                </div>

                                 <div class="row"><!-- quanr ly hinh anh -->
                    
                                    <div class="col-lg-12 col-md-12">
                                            <div class="row upload-area" >
                                                <div class="col-lg-10 col-md-10">
                                                    <input type="hidden" value="" name="image" id="image-url"/>
                                                    <div class="img-show">
                                                        <img class="product-img"  src="{{ asset('') }}/{{ $brand->image }}" width="150" height="150" />
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-2 ">
                                                    <a href="javascript:void(0);" class="btn btn-info pull-right " id="ckfinder-btn">Thêm ảnh</a>
                                                </div>
                                            </div>
                                        <hr />
                                    </div>
                                </div> <!-- /image -->

                                <div class="form-group">
                                   
                                    <label class="checkbox-inline"><input @if ($brand->available) checked  @endif name="available" type="checkbox">Available?</label>
                                </div>

                                <hr />


                                <div class="btn-group pull-left">
                                    <button type="submit" class="btn btn-info">Lưu</button>
                                </div>
                                <div class="clearfix"></div>

                            </form>


                        </div>

                    </div>

            </div>
        </div>
    </div>

@endsection


<!--------------- custom page css -------------->
@section('page-css')
    <style type="text/css">
        .error {
            color:red;
            font-weight: normal;
        }
    </style>

@endsection

<!--------------- custom header javascript-------->
@section('page-header-js')

@endsection

<!--------------- custom footer javascript-------->
@section('page-footer-js')
    <script src="{{ asset('assets/plugins/editor/ckfinder/ckfinder.js') }}"></script>
     <script type="text/javascript">
        var button1 = document.getElementById( 'ckfinder-btn' );

        button1.onclick = function() {
            selectFileWithCKFinder( 'image-url' );
        };

        function selectFileWithCKFinder( elementId ) {
            CKFinder.modal( {
                chooseFiles: true,
                width: 800,
                height: 600,
                onInit: function( finder ) {
                    finder.on( 'files:choose', function( evt ) {
                        var file = evt.data.files.first();
                        var output = document.getElementById( elementId );
                        output.value = file.getUrl();
                        //show images
                        $('.img-show').html('<img class="product-img"  src="{{ asset('') }}/'+ file.getUrl() +'" width="150" height="150" />');
                    } );
                    finder.on( 'file:choose:resizedImage', function( evt ) {
                        var output = document.getElementById( elementId );
                        output.value = file.getUrl();
                        //show images
                        $('.img-show').append('<img class="product-img"  src="{{ asset('') }}/'+ file.getUrl() +'" width="150" height="150" />');
                    } );
                }
            } );
        }
    </script>

    <!-- jquery validation -->
    <script src="{{ asset('assets/plugins/jquery-validation') }}/jquery.validate.min.js"></script>
    <script src="{{ asset('assets/plugins/jquery-validation') }}/localization/messages_vi.min.js"></script>
    <script type="text/javascript">
        function confirm_delete() {
            var confirm_delete = confirm('Xóa brand?');
            return confirm_delete;
        }
    </script>
    <!-- ajax create -->

@endsection
@extends('admin.layouts.master')

<!--------------- page title --------------- -->
@section('title')
    Edit Stock
@endsection

<!--------------- start content ----------------->
@section('content')

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12 col-md-12">
            <div class="box">

                    <!-- start imput here -->
                    <div class="row">
                        <!-- for create brand -->
                        <div class="col-lg-3 col-md-3">
                                <div class="help-block">
                                    <h4>Thông tin cơ bản</h4>
                                    <p style="color:#cecece;">Infomation of Stock</p>
                                </div>
                        </div>

                        <div class="col-md-9">
                            <form role="form" id="brand-form" style="padding:10px" method="POST" action="{{ route('stock.edit') }}">
                                {{ csrf_field() }}
                                <div class="form-group @if( $errors->has('name') ) has-error @endif">
                                    <label for="inputName">Name</label>
                                    <input required id="name" name="name"  type="text" value="{{ $stock->name }}" class="form-control" placeholder="Stock 's Name" />
                                    <span class="help-block">{{ $errors->first('name') }}</span>
                                </div>
                                
                                <div class="form-group @if( $errors->has('address') ) has-error @endif">
                                    <label for="inputName">Address</label>
                                    <input required id="address" name="address"  type="text" value="{{ $stock->address }}" class="form-control" placeholder="Address" />
                                    <span class="help-block">{{ $errors->first('name') }}</span>
                                </div>
                                <hr />
                                <div class="btn-group pull-left">
                                    <button type="submit" class="btn btn-info">Lưu</button>
                                </div>
                                <div class="clearfix"></div>

                            </form>
                        </div>

                    </div>

            </div>
        </div>
    </div>

@endsection


<!--------------- custom page css -------------->
@section('page-css')
    <style type="text/css">
        .error {
            color:red;
            font-weight: normal;
        }
    </style>

@endsection

<!--------------- custom header javascript-------->
@section('page-header-js')

@endsection

<!--------------- custom footer javascript-------->
@section('page-footer-js')
    <!-- jquery validation -->
    <script src="{{ asset('assets/plugins/jquery-validation') }}/jquery.validate.min.js"></script>
    <script src="{{ asset('assets/plugins/jquery-validation') }}/localization/messages_vi.min.js"></script>
    <!-- ajax create -->

@endsection
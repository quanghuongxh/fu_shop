@extends('admin.layouts.master')

<!--------------- page title --------------- -->
@section('title')
    Create Stock manager
@endsection

<!--------------- start content ----------------->
@section('content')

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12 col-md-12">
            <div class="box">

                    <!-- start imput here -->
                    <div class="row">
                        <!-- for create brand -->
                        <div class="col-lg-3 col-md-3">
                                <div class="help-block">
                                    <h4>Thông tin cơ bản</h4>
                                    <p style="color:#cecece;">Infomation of brand</p>
                                </div>
                        </div>

                        <div class="col-md-9">
                            <form role="form" id="brand-form" style="padding:10px" method="POST" action="{{ route('stock.store') }}">
                                {{ csrf_field() }}

                                <div class="form-group @if( $errors->has('name') ) has-error @endif">
                                    <label for="inputName">Name</label>
                                    <input required id="name" name="name"  type="text" value="{{ old('name') }}" class="form-control" placeholder="Stock 's Name" />
                                    <span class="help-block">{{ $errors->first('name') }}</span>
                                </div>
                                
                                <div class="form-group @if( $errors->has('address') ) has-error @endif">
                                    <label for="inputName">Address</label>
                                    <input required id="address" name="address"  type="text" value="{{ old('address') }}" class="form-control" placeholder="Address" />
                                    <span class="help-block">{{ $errors->first('name') }}</span>
                                </div>
                                <hr />
                                <div class="btn-group pull-left">
                                    <button type="submit" class="btn btn-info">Lưu</button>
                                </div>
                                <div class="clearfix"></div>

                            </form>


                        </div>

                    </div>

            </div>
        </div>
    </div>

@endsection


<!--------------- custom page css -------------->
@section('page-css')
    <style type="text/css">
        .error {
            color:red;
            font-weight: normal;
        }
    </style>

@endsection

<!--------------- custom header javascript-------->
@section('page-header-js')

@endsection

<!--------------- custom footer javascript-------->
@section('page-footer-js')
    <!-- jquery validation -->
    <script src="{{ asset('assets/plugins/jquery-validation') }}/jquery.validate.min.js"></script>
    <script src="{{ asset('assets/plugins/jquery-validation') }}/localization/messages_vi.min.js"></script>
    <script type="text/javascript">
        function confirm_delete() {
            var confirm_delete = confirm('Delete stock?');
            return confirm_delete;
        }
    </script>
    <!-- ajax create -->

@endsection
@extends('admin.layouts.master')
<!--------------- page title --------------- -->
@section('title')
    Stock manager
@endsection

<!--------------- start content ----------------->
@section('content')

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Stock manager</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">

                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">

                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                <a href="{{ route('stock.create') }}" class="btn btn-info">Tạo mới</a>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>ID</th>
                            <th>Tên</th>
                            <th>Thao tác</th>
                        </tr>
                        @if ( isset($list_stock) && count($list_stock) >  0)

                            @foreach ($list_stock as $stock )
                                <tr>
                                    <td> {{ $stock->id }}</td>
                                    <td> <a href="{{ route('stock.view', $stock->id) }}"> {{ $stock->name }} </a> </td>
                                    <td>

                                        <a href="{{ route('stock.view', $stock->id) }}" class="btn btn-xs btn-info"><i class="fa  fa-edit"></i> </a>
                                        <form action="{{ route('stock.delete', $stock->id) }}" method="POST" onsubmit="return confirm_delete();">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="delete-btn btn btn-xs btn-warning"><i class="fa fa-remove"></i> </button>
                                        </form>
                                      
                                    </td>

                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
                <!-- /.box-body -->
                {{ $list_stock->links() }}
            </div>
            <!-- /.box -->
        </div>
    </div>

@endsection


<!--------------- custom page css -------------->
@section('page-css')

@endsection

<!--------------- custom header javascript-------->
@section('page-header-js')
    <script>
        function confirm_delete() {
            var confirm_delete = confirm('Xóa chuyên mục');
            return confirm_delete;
        }
    </script>
@endsection

<!--------------- custom footer javascript-------->
@section('page-footer-js')
   

@endsection
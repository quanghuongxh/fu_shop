@extends('admin.layouts.master')

<!--------------- page title --------------- -->
@section('title')
    Tạo danh mục
@endsection

<!--------------- start content ----------------->
@section('content')

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12 col-md-12">
            <div class="box">

                <!-- start imput here -->
                <div class="row">
                    <!-- for create category -->

                    <div class="col-md-12">
                        <form role="form" id="category-form" style="padding:10px" method="POST" action="{{ route('category.edit') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $category->id }}" />

                            <div class="form-group @if( $errors->has('name') ) has-error @endif">
                                <label for="inputName">Tên</label>
                                <input id="name" name="name"  type="text" value="{{ $category->name }}" class="form-control" placeholder="Tên" />
                                <span class="help-block">{{ $errors->first('name') }}</span>
                            </div>

                            <div class="form-group">
                                <label for="inputDesc">Mô tả</label>
                                <textarea id="description" class="form-control" rows="9" name="description">{{ $category->description }}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Danh mục cha</label>
                                <select id="parent_id" class="form-control" name="parent_id">
                                    <option @if (! $parent_category) selected @endif value="0">Không có</option>
                                    @foreach ($list_categories as $cat)
                                        <option @if ($parent_category && $cat->id == $parent_category->id) selected @endif value="{{ $cat->id }}">{{ $cat->name }}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="form-group">
                                <label   class="checkbox-inline"><input @if ($category->is_feature) checked @endif id="is_featured" name="is_feature" type="checkbox" value="1">Danh mục nổi bật</label>
                                <label   class="checkbox-inline"><input @if ($category->available) checked @endif id="available" name="available" type="checkbox"  value="1">Hiển thị</label>
                            </div>

                            <hr />


                            <div class="btn-group pull-left">
                                <button type="submit" class="btn btn-info">Lưu</button>
                            </div>
                            <div class="clearfix"></div>

                        </form>


                    </div>

                </div>

            </div>
        </div>
    </div>

@endsection


<!--------------- custom page css -------------->
@section('page-css')
    <style type="text/css">
        .error {
            color:red;
            font-weight: normal;
        }
    </style>

@endsection

<!--------------- custom header javascript-------->
@section('page-header-js')

@endsection

<!--------------- custom footer javascript-------->
@section('page-footer-js')

    <!-- jquery validation -->
    <script src="{{ asset('assets/plugins/jquery-validation') }}/jquery.validate.min.js"></script>
    <script src="{{ asset('assets/plugins/jquery-validation') }}/localization/messages_vi.min.js"></script>

    <!-- ajax create -->

@endsection
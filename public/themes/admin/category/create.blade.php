@extends('admin.layouts.master')

<!--------------- page title --------------- -->
@section('title')
    Tạo danh mục
@endsection

<!--------------- start content ----------------->
@section('content')

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12 col-md-12">
            <div class="box">

                    <!-- start imput here -->
                    <div class="row">
                        <!-- for create category -->

                        <div class="col-md-5">
                            <form role="form" id="category-form" style="padding:10px" method="POST" action="{{ route('category.store') }}">
                                {{ csrf_field() }}

                                <div class="form-group @if( $errors->has('name') ) has-error @endif">
                                    <label for="inputName">Tên</label>
                                    <input id="name" name="name"  type="text" value="{{ old('name') }}" class="form-control" placeholder="Tên" />
                                    <span class="help-block">{{ $errors->first('name') }}</span>
                                </div>

                                <div class="form-group">
                                    <label for="inputDesc">Mô tả</label>
                                    <textarea id="description" class="form-control" rows="9" name="description">{{ old('description') }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label>Danh mục cha</label>
                                    <select id="parent_id" class="form-control" name="parent_id">
                                        <option selected value="0">Không có</option>
                                        @foreach ($all_category as $cat)
                                            @if ( $cat->id != 1 )
                                            <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>


                                <div class="form-group">
                                    <label  class="checkbox-inline"><input id="is_featured" name="is_feature" type="checkbox" value="1">Danh mục nổi bật</label>
                                    <label  class="checkbox-inline"><input id="available" name="available" type="checkbox" checked value="1">Hiển thị</label>
                                </div>

                                <hr />


                                <div class="btn-group pull-left">
                                    <button type="submit" class="btn btn-info">Lưu</button>
                                </div>
                                <div class="clearfix"></div>

                            </form>


                        </div>



                        <!-- for display categries -->
                        <div class="col-md-7" style="background-color: #e0e0e0;">
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover" id="category-table">
                                    <tr>
                                        <th>ID</th>
                                        <th>Tên</th>

                                        <th>Trạng thái</th>
                                        <th>Thao tác</th>
                                    </tr>
                                    @if ( count($list_categories) >  0)

                                        @foreach ($list_categories as $category )
                                            <tr>
                                                <td> {{ $category->id }}</td>
                                                <td> <a href="{{ route('category.view', $category->id) }}"> {{ $category->name }} </a> </td>

                                                <td>
                                                    @if ($category->available)
                                                        <span class="label label-success">
                                                <i class="fa fa-check"></i>
                                            </span>
                                                    @else
                                                        <span class="label label-fail">
                                                <i class="fa fa-close"></i>
                                            </span>
                                                    @endif
                                                </td>

                                                <td>
                                                    @if ($category->id != 1)

                                                    <a href="{{ route('category.view', $category->id) }}" class="btn btn-xs btn-info"><i class="fa  fa-edit"></i> </a>
                                                    <form action="{{ route('category.delete', $category->id) }}" method="POST" onsubmit="return confirm_delete();">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="delete-btn btn btn-xs btn-warning"><i class="fa fa-remove"></i> </button>
                                                    </form>
                                                    @endif

                                                </td>

                                            </tr>
                                        @endforeach
                                    @endif
                                </table>
                            </div>
                            <!-- /.box-body -->
                            {{ $list_categories->links() }}

                        </div>

                    </div>

            </div>
        </div>
    </div>

@endsection


<!--------------- custom page css -------------->
@section('page-css')
    <style type="text/css">
        .error {
            color:red;
            font-weight: normal;
        }
    </style>

@endsection

<!--------------- custom header javascript-------->
@section('page-header-js')

@endsection

<!--------------- custom footer javascript-------->
@section('page-footer-js')

    <!-- jquery validation -->
    <script src="{{ asset('assets/plugins/jquery-validation') }}/jquery.validate.min.js"></script>
    <script src="{{ asset('assets/plugins/jquery-validation') }}/localization/messages_vi.min.js"></script>
    <script type="text/javascript">
        function confirm_delete() {
            var confirm_delete = confirm('Xóa category?');
            return confirm_delete;
        }
    </script>
    <!-- ajax create -->

@endsection
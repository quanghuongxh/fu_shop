@include('admin.partials.header')
@include('admin.partials.sidebar')


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            @yield('title')
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">@yield('title')</li>

        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!------ alert notification area ----->
        @if (session('noti_fail'))
            <script>
                notify = "{{ session('noti_fail') }}";
                swal({
                    title: "Lỗi!",
                    text: notify,
                    type: "error",
                    confirmButtonText: "Đóng"
                });
            </script>
        @endif

        @if (session('noti_success'))
            <script>
                notify = "{{ session('noti_success') }}";
                swal({
                    title: "Thành công!",
                    text: notify,
                    type: "success",
                    confirmButtonText: "Đóng"
                });
            </script>
        @endif

        @if (session('noti_normal'))
            <script>
                notify = "{{ session('noti_normal') }}";
                swal({
                    title: "Thông báo!",
                    text: notify,
                    type: "info",
                    confirmButtonText: "Đóng"
                });
            </script>
        @endif
    @yield('content')

    </section>

</div>
<!-- /.content-wrapper -->
<footer class="main-footer">

</footer>



<!-- ./wrapper -->
@include('admin.partials.footer')

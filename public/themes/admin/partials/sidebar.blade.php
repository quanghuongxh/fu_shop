<!-- Left side column. contains the logo and sidebar -->
<?php $current_page = Route::currentRouteName(); ?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('assets') }}/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ session('user_name') }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Shop Management </li>

            <li @if ($current_page == 'dashboard' ) class="active" @endif>
                <a href="{{ route('dashboard') }}">
                    <i class="fa fa-dashboard"></i> Dashboard
                </a>
            </li>


            <li @if ($current_page == 'order.list' ) class="active" @endif>
                <a href="{{ route('order.list') }}">
                    <i class="fa fa-files-o"></i>
                    <span>Order</span><small class="label pull-right bg-blue">17</small>
                </a>
            </li>

            <li @if ($current_page == 'product.list' ) class="active" @endif>
                <a href="{{ route('product.list') }}">
                    <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                    <span>Product</span>
                </a>
            </li>

            <li @if ($current_page == 'category.list' ) class="active" @endif>
                <a href="{{ route('category.list') }}">
                    <i class="fa fa-suitcase" aria-hidden="true"></i>
                    <span>Category</span>
                </a>
            </li>

            <li @if ($current_page == 'brand.list' ) class="active" @endif>
                <a href="{{  route('brand.list') }}">
                    <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                    <span>Brand</span>
                </a>
            </li>

            <li @if ($current_page == 'stock.list' ) class="active" @endif>
                <a href="{{ route('stock.list') }}">
                    <i class="fa fa-taxi" aria-hidden="true"></i>
                    <span>Stock</span>
                </a>
            </li>

            <li @if ($current_page == 'product.list' ) class="active" @endif>
                <a href="#">
                    <i class="fa fa-star"></i>
                    <span>Review</span>
                </a>
            </li>

            <li @if ($current_page == 'customer.list' ) class="active" @endif>
                <a href="{{ route('customer.list') }}">
                    <i class="fa fa-user"></i>
                    <span>Customer</span>
                </a>
            </li>

            {{-- <li class="treeview">
                <a href="#">
                    <i class="fa fa-wrench"></i>
                    <span>System</span>
                </a>
            </li> --}}
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
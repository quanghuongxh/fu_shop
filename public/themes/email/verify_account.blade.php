<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Verify Your Email Address</h2>

<div>
    Thanks for creating an account with the verification of Furniture Shop.
    Please follow the link below to verify your email address
    <a href=" {{ route('customer.activate', ['user_id' => $user_id, 'confirmationCode' => $confirmation_code]) }}"> click here</a>
    <br/>

</div>

</body>
</html>
@extends('shop.layout.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12"><a class="continue-shopping"
          style="padding:2em;background: url({{URL::to('assets/shop/images/continue-shopping.png')}}) no-repeat;height: 23px;display: block;margin-top: 33px;font-family: Lato;font-size: 14px;font-weight: bold;letter-spacing: 2px;line-height: 1.3em;text-decoration: none;text-transform: uppercase;color: #58585C;"
          href="{{ route('index') }}"></a></div>
      </div>
      <div id="charge-error" class="alert alert-danger {{ !Session::has('error') ? 'hidden' : '' }}">
        {{ Session::get('error') }}
    </div>
    <form action="{{ route('checkout') }}" method="post" id="checkout-form">
        <div class="row" style="padding: 1em 0">
            <div class="col-md-4">
                <h4>EXPRESS CHECKOUT</h4>
                <h5>1.BILLING ADDRESS</h5>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" id="name" name="name" value="{{ old('name') }}" class="form-control" style="width: 80%">
                            @if($errors->has('name'))
                            <p class="help-block">
                                {{ $errors->first('name') }}
                            </p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="number" id="phone" name="phone" class="form-control" value="{{ old('phone') }}" style="width: 80%">
                            @if($errors->has('phone'))
                            <p class="help-block">
                                {{ $errors->first('phone') }}
                            </p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" id="email" name="email" class="form-control" value="{{ old('email') }}" style="width: 80%">
                            @if($errors->has('email'))
                            <p class="help-block">
                                {{ $errors->first('email') }}
                            </p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" id="address" name="address" class="form-control" value="{{ old('address') }}" style="width: 80%">
                            @if($errors->has('address'))
                            <p class="help-block">
                                {{ $errors->first('address') }}
                            </p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="border-left: 1px dotted #080808;border-right: 1px dotted #080808">
                <h5>2. PAYMENT METHOD</h5>
                <div class="col-xs-12"><label><input type="radio"
                 name="payment-method" value="Creadit or Debit Card" checked><span> Credit Card</span></label>
                 <img
                 src="{{URL::to('assets/shop/images/mastercard-0bd5208385675abeebc0e68c30b7e64f50355751c376cd1859aa152450903f5e.png')}}"
                 style="height: 25px; margin-left: 5px; float: right; display: inline-block; margin-top: -2px;"><img
                 src="{{URL::to('assets/shop/images/visa-eba96747467d276eb4dde29bfe34051a4fa262c2596b9a138a8cdf50d75d0dad.png')}}"
                 style="height: 25px; margin-left: 5px; float: right; display: inline-block; margin-top: -2px;"><img
                 src="{{URL::to('assets/shop/images/amex-3e277ea97b5d89f17e48b29a38345308af1aaf875e779dac33cc7153b9954176.png')}}"
                 style="height: 25px; margin-left: 5px; float: right; display: inline-block; margin-top: -2px;">
                 <p>
                 Online payment using Visa / Mastercard / JCB. Fast, convenient and absolutely safe.</p>
             </div>
             <div class="col-xs-12">
                <div class="form-group">
                    <label for="card-name">Card Holder Name</label>
                    <input type="text" id="card-name" name="card-name" class="form-control" value="{{ old('card-name') }}" style="width: 80%">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="card-number">Credit Card Number</label>
                    <input type="text" id="card-number" name="card-number" class="form-control" value="{{ old('card-number') }}" style="width: 80%">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-5">
                        <div class="form-group">
                            <label for="card-expiry-month">Expiration Month</label>
                            <input type="text" id="card-expiry-month" name="card-expiry-month" class="form-control" value="{{ old('card-expiry-month') }}" style="width: 100%">
                        </div>
                    </div>
                    <div class="col-xs-5">
                        <div class="form-group">
                            <label for="card-expiry-year">Expiration Year</label>
                            <input type="text" id="card-expiry-year" name="card-expiry-year" class="form-control" value="{{ old('card-expiry-year') }}" style="width: 100%">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="card-cvc">CVC</label>
                    <input type="text" id="card-cvc" name="card-cvc" class="form-control" value="{{ old('card-cvc') }}">
                </div>
            </div>
            <div class="col-xs-12">
                <label><input type="radio" disabled 
                  name="payment-method" value="Cash on Delivery"><span> CASH PAYMENT</span></label>
                  <img src="{{URL::to('assets/shop/images/icon-cod.png')}}" alt=""  style="height: 30px; margin-left: 5px; float: right; display: inline-block; margin-top: -2px;">

                  <p>Pay directly to cashier for FurnitureShop delivery staff upon delivery.</p>
              </div>
          </div>
          <div class="col-md-4">
            <h5>3. REVIEW YOUR ORDER</h5>
            {{ csrf_field() }}
            <button type="submit" class="btn btn-proceed-checkout" style="text-decoration: none; display: inline-block;padding: 7px 17px;border-radius: 999px;margin-right: 8px;font-size: 12px;text-transform: uppercase;font-weight: 700;letter-spacing: 0.5px;cursor: pointer;line-height: inherit;">
                <span>Place Order</span>
            </button>
        </div>
    </div>
</form>
</div>
@endsection
@section('script')
<script type="text/javascript" src="{{URL::to('assets/shop/js/stripe.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/shop/js/checkout.js')}}"></script>
@endsection
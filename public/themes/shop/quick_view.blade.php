@extends('shop.layout.view')

<div style="background-color: rgb(119, 119, 119); opacity: 0.7; cursor: pointer; height: 1024px; display: block;" id="fancybox-overlay"></div>
<div style="top: 15%; display: block;" id="fancybox-wrap">
    <div id="fancybox-outer">
        <div style="border-width: 10px; width: 1170px; height: auto;" id="fancybox-content">
            <div style="width:auto;height:auto;overflow: auto;position:relative;">
                <div class="product-view">
                    <div class="product-essential">
                        <form action="#" method="post" id="product_addtocart_form">
                            <input name="form_key" value="6UbXroakyQlbfQzK" type="hidden">
                            <div class="product-img-box col-lg-5 col-sm-5 col-xs-12">
                                <div class="new-label new-top-left"> New </div>
                                <div class="product-image">
                                    <div class="product-full"> <img id="product-zoom" src="{{ URL::asset((json_decode($product->images))[0]) }}" data-zoom-image="{{ URL::asset((json_decode($product->images))[0]) }}" alt="product-image"/> </div>
                                    <div class="more-views">
                                        <div class="slider-items-products">
                                            <div id="gallery_01" class="product-flexslider hidden-buttons product-img-thumb">
                                                <div class="slider-items slider-width-col4 block-content">
                                                    <div class="more-views-items"> <a href="#" data-image="assets/shop/products-images/product2.jpg" data-zoom-image="assets/shop/products-images/product2.jpg"> <img id="product-zoom"  src="{{URL::to('assets/shop/products-images/product2.jpg')}}" alt="product-image"/> </a></div>
                                                    <div class="more-views-items"> <a href="#" data-image="assets/shop/products-images/product3.jpg" data-zoom-image="assets/shop/products-images/product3.jpg"> <img id="product-zoom"  src="{{URL::to('assets/shop/products-images/product3.jpg')}}" alt="product-image"/> </a></div>
                                                    <div class="more-views-items"> <a href="#" data-image="assets/shop/products-images/product4.jpg" data-zoom-image="assets/shop/products-images/product4.jpg"> <img id="product-zoom"  src="{{URL::to('assets/shop/products-images/product4.jpg')}}" alt="product-image"/> </a></div>
                                                    <div class="more-views-items"> <a href="#" data-image="assets/shop/products-images/product5.jpg" data-zoom-image="assets/shop/products-images/product5.jpg"> <img id="product-zoom"  src="{{URL::to('assets/shop/products-images/product5.jpg')}}" alt="product-image"/> </a> </div>
                                                    <div class="more-views-items"> <a href="#" data-image="assets/shop/products-images/product6.jpg" data-zoom-image="assets/shop/products-images/product6.jpg"> <img id="product-zoom"  src="{{URL::to('assets/shop/products-images/product6.jpg')}}" alt="product-image" /> </a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end: more-images -->
                            </div>
                            <div class="product-shop col-lg-7 col-sm-7 col-xs-12">
                                <div class="product-name">
                                    <h1>{{ $product->name }}</h1>
                                </div>
                                <div class="ratings">
                                    <div class="rating-box">
                                        <div style="width:100%" class="rating"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> </div>
                                    </div>
                                    <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Your Review</a> </p>
                                </div>
                                <div class="price-block">
                                    <div class="price-box">
                                        <p class="special-price"> <span class="price-label">Special Price</span> <span id="product-price-48" class="price"> ${{ $product->sale_price }} </span> </p>
                                        <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> ${{ $product->sell_price }} </span> </p>
                                        <p class="availability in-stock pull-right"><span>In Stock</span></p>
                                    </div>
                                </div>
                                <div class="short-description">
                                    <h2>Quick Overview</h2>
                                    <p>{{ $product->short_desc }}</p>
                                </div>
                                <div class="add-to-box">
                                    <div class="add-to-cart">
                                        <div class="pull-left">
                                            <div class="custom pull-left">
                                                <button onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;" class="reduced items-count" type="button"><i class="fa fa-minus">&nbsp;</i></button>
                                                <input type="text" class="input-text qty" title="Qty" value="1" maxlength="12" id="qty" name="qty">
                                                <button onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" class="increase items-count" type="button"><i class="fa fa-plus">&nbsp;</i></button>
                                            </div>
                                        </div>
                                        <button onClick="productAddToCartForm.submit(this)" class="button btn-cart" title="Add to Cart" type="button">Add to Cart</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--product-view-->

            </div>
        </div>
        <a style="display: inline;" id="fancybox-close" href="{!! route('index') !!}"></a> </div>
</div>
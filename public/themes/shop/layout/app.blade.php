
<!DOCTYPE html>
<html lang="vi">
<head>
	<title>@yield('title')</title>
	@include('shop.partials.head')

	@yield('style')
</head>
<body class="common-home">
	@include('shop.partials.header')
	
	{{-- @include('shop.partials.error') --}}
	
	@yield('content')
	
	@include('shop.partials.footer')
	
	@include('shop.partials.javascript')
	@yield('script')
</body>
</html>

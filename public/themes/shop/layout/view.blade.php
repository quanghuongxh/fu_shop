
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>

    <!-- Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS Style -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/font-awesome.css')}}" media="all">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/simple-line-icons.css')}}" media="all">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/owl.theme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/jquery.mobile-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/style.css')}}" media="all">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/blog.css')}}" media="all">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/flexslider.css')}}" media="all">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/fancybox.css')}}" media="all">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,600,500,700,800' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
</head>

<body class="cms-index-index">
@yield('content')
<!-- JavaScript -->
<script type="text/javascript" src="{{URL::to('assets/shop/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/shop/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/shop/js/common.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/shop/js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/shop/js/jquery.mobile-menu.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/shop/js/jquery.flexslider.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/shop/js/cloud-zoom.js')}}"></script>
</body>
</html>

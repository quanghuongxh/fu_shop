@extends('shop.layout.app')
@section('style')
<style>
img.avatar {
  border: 1px solid #eee;
  width: 35%;
}

.only-bottom-margin {
  margin-top: 0px;
}

.activity-mini {
  padding-right: 15px;
  float: left;
}
</style>
@endsection
@section('content')
<div class="row">
  <div class="col-md-8 col-md-offset-2">
    <br>
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12 lead"><h4>User profile</h4><hr></div>
              </div>
              <div class="row">
                <div class="col-md-4 text-center">
                  <img class="img-circle avatar avatar-original" style="-webkit-user-select:none; 
                  display:block; margin:auto;" src="{{ URL::to('assets/shop/images/default-avatar.png') }}">
                </div>
                <div class="col-md-8">
                  <div class="row">
                    <div class="col-md-12">
                      <h1 class="only-bottom-margin">{{ $user->first_name }} {{ $user->last_name }} </h1>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <span class="text-muted">Email:</span> {{ $user->email }}<br>
                      <span class="text-muted">Phone:</span> {{ $user->phone }}<br>
                      <span class="text-muted">Address:</span> {{ $user->address }}<br><br>
                      <small class="text-muted">Created: {{ $user->created_at }}</small>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <hr><button class="btn btn-default pull-right btn-edit"><i class="glyphicon glyphicon-pencil"></i> Edit</button>
                  <button class="btn btn-default pull-right btn-cancel" style="display: none"><i class="glyphicon glyphicon-remove"></i> Cancel</button>
                  <button class="btn btn-default pull-right btn-update" style="display: none"><i class="glyphicon glyphicon-ok"></i> Update</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
  $(document).ready(function () {
    $('.btn-edit').click(function(){
    $('.btn-edit').removeAttr('style').hide();
    $('.btn-cancel').removeAttr('style').show();
    $('.btn-update').removeAttr('style').show();
    });

     $('.btn-cancel').click(function(){
    $('.btn-edit').removeAttr('style').show();
    $('.btn-cancel').removeAttr('style').hide();
    $('.btn-update').removeAttr('style').hide();
    });
  });
  </script>
  @endsection
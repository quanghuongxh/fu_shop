@section('title')
Shopping online - Buy online onn fushop.vn
@endsection

@extends('shop.layout.app')

@section('content')
<div id="content">
  <!-- Slider -->
  <div id="thmsoft-slideshow" class="thmsoft-slideshow">
    <div class="">
      <div>
        <div class="thm_topsection">
          <div>
            <div id='rev_slider_4_wrapper' class='rev_slider_wrapper fullwidthbanner-container'>
              <div id='rev_slider_4' class='rev_slider fullwidthabanner'>
                <ul>
                  <li data-transition='random' data-slotamount='7' data-masterspeed='1000' data-thumb='{{URL::to('assets/shop/images/slider-img1.jpg')}}'><img src="{{URL::to('assets/shop/images/slider-img1.jpg')}}" alt="slide-img" data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat' />
                    <div class="info">
                      <div class='tp-caption LargeTitle sfl  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3;max-width:auto;max-height:auto;white-space:nowrap;'><span>New Arrivals</span> </div>
                      <div class='tp-caption Title sft  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1450' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;max-width:auto;max-height:auto;white-space:nowrap;'>Big Sale 20% off</div>
                      <div class='tp-caption sfb  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;max-width:auto;max-height:auto;white-space:nowrap;'><a href='#' class="buy-btn">See product</a> </div>
                    </div>
                  </li>
                  <li data-transition='random' data-slotamount='7' data-masterspeed='1000' data-thumb='{{URL::to('assets/shop/images/slider-img2.jpg')}}'><img src="{{URL::to('assets/shop/images/slider-img2.jpg')}}" alt="slide-img" data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat' />
                    <div class="info1">
                      <div class='tp-caption LargeTitle sfl  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3;max-width:auto;max-height:auto;white-space:nowrap;'>Wooden Sofa</div>
                      <div class='tp-caption Title sft  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;max-width:auto;max-height:auto;white-space:nowrap;'>Get 30% Discount</div>
                      <div class='tp-caption sfb  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;max-width:auto;max-height:auto;white-space:nowrap;'><a href='#' class="buy-btn">Learn More</a> </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end Slider -->

<!-- Banner Blog -->

<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="banner-container">
        <div class="banner-inner">
          <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
              <div class="figure-content"><a> <img alt="Blog" src="{{URL::to('assets/shop/images/ban-1.jpg')}}"> </a> </div>
              <div>
                <div class="center white">
                  <h2 class="light h2">Modern</h2>
                  <p class="h4">Dining Table</p>
                  <button class="btn-learn">Learn More</button>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
              <div class="figure-content"><a> <img alt="Blog" src="{{URL::to('assets/shop/images/ban-2.jpg')}}"> </a> </div>
              <div>
                <div class="center">
                  <h2 class="light h2">Beautiful</h2>
                  <p class="h4">Classic Chair</p>
                  <button class="btn-learn">Add to cart</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="banner-container">
        <div class="banner-inner">
          <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
              <div class="figure-content"><a> <img alt="Blog" src="{{URL::to('assets/shop/images/banner3.jpg')}}"> </a> </div>
              <div>
                <div class="center">
                  <h2 class="light h2">Creative</h2>
                  <p class="h4">Wooden Lamp</p>
                  <button class="btn-learn">Add to cart</button>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
              <div class="figure-content"> <a><img alt="Blog" src="{{URL::to('assets/shop/images/banner4.jpg')}}"> </a> </div>
              <div>
                <div class="center">
                  <h2 class="light h2">Decorative</h2>
                  <p class="h4">Sofa Collection </p>
                  <button class="btn-learn">Learn More</button>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
              <div class="figure-content"><a> <img alt="Blog" src="{{URL::to('assets/shop/images/banner5.jpg')}}"> </a> </div>
              <div>
                <div class="center">
                  <h2 class="light h2">Luxury</h2>
                  <p class="h4">Storage Stools</p>
                  <button class="btn-learn">View All</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- End Banner Blog -->
<div class="container">
  <div class="row">
    <!-- Popular Products -->
    <div class="col-md-12">
      <div class="bestsell-pro">
        <div class="slider-items-products">
          <div class="bestsell-block">
            <div class="h2 products-section-title">Popular Products</div>
            <div id="bestsell-slider" class="product-flexslider hidden-buttons">
              <div class="slider-items slider-width-col4 products-grid block-content">
                @foreach($popularProduct as $product)
                <div class="item">
                  <div class="item-inner">
                    <div class="productborder">
                      <div class="item-img">
                        <div class="item-img-info"> <a class="product-image" title="" href="{!! route('product.detail', ['product_id' => $product->id, 'brand_id' => $product->brand_id]) !!}h"> <img alt=""  src="{{  (json_decode($product->images))[0] }}"> </a>
                          <div class="new-label new-top-right">new</div>
                          <div class="quickview top-right"> <a class="link-quickview" href="{!! route('quick.view',['product_id' => $product->id]) !!}"> <i class="fa fa-search" aria-hidden="true"></i></a> </div>
                          <div class="box-hover">
                            <ul class="add-to-links">
                              <li><a class="link-compare" href="compare.html"></a> </li>
                              <li><a class="add-to-cart" href="{!! route('product.addToCart', ['id' => $product->id]) !!}"></a> </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="right-block">
                        <div class="item-info">
                          <div class="info-inner">
                            <div class="item-title"> <a title="product lapen casen" href="{!! route('product.detail', ['product_id' => $product->id, 'brand_id' => $product->brand_id]) !!}h">{{ $product->name }}</a> </div>
                            <div class="item-content">
                              <p class="product-manufacturer">{{ $product->brand_name }}</p>
                            </div>
                            <div class="item-price">
                              <div class="price-box"> <span class="regular-price"> <span class="price">${{ $product->sell_price }}</span> </span> </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--Popular Products end-->
<!--home banner-->
<div class="bottom-banner-section">
  <div class="container"> <img alt="banner" src="{{URL::to('assets/shop/images/bottom-banner.jpg')}}"> </div>
</div>
</div>
<!--home banner end-->
<!--3 Tabs-->
<div class="main-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="new-product">
          <div class="h2 products-section-title">New Products</div>
          @foreach($newestProduct as $product)
          <div class="item">
            <div class="item-inner">
              <div class="item-img col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="item-img-info"> <a class="product_image image-wrapper" href="{!! route('product.detail', ['product_id' => $product->id, 'brand_id' => $product->brand_id]) !!}"> <img class="front_image" alt="Pro Image" src="{{ URL::asset((json_decode($product->images))[0]) }}"/></a> </div>
              </div>
              <div class="item-info col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div class="info-inner">
                  <div class="item-title"> <a title="sample" href="{!! route('product.detail', ['product_id' => $product->id, 'brand_id' => $product->brand_id]) !!}"> {{ $product->name }} </a> </div>
                  <div class="item-content">
                    <div class="rating">
                      <div class="ratings">
                        <div class="rating-box">
                          <div style="width:80%" class="rating"></div>
                        </div>
                      </div>
                    </div>
                    <div class="item-price">
                      <div class="price-box"> <span class="regular-price"> <span class="price">${{ $product->sell_price }}</span> </span> </div>
                    </div>
                    <div class="action"> </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach
          {{--<div class="item">--}}
            {{--<div class="item-inner">--}}
              {{--<div class="item-img col-lg-4 col-md-4 col-sm-4 col-xs-12">--}}
                {{--<div class="item-img-info"> <a class="product_image image-wrapper" href="{!! route('product.detail', ['product_id' => $product->id, 'brand_id' => $product->brand_id]) !!}h"> <img class="front_image" alt="Pro Image" src="assets/shop/products-images/product2.jpg"></a> </div>--}}
              {{--</div>--}}
              {{--<div class="item-info col-lg-8 col-md-8 col-sm-8 col-xs-12">--}}
                {{--<div class="info-inner">--}}
                  {{--<div class="item-title"> <a title="sample" href="{!! route('product.detail', ['product_id' => $product->id, 'brand_id' => $product->brand_id]) !!}h"> Kismos Chair </a> </div>--}}
                  {{--<div class="item-content">--}}
                    {{--<div class="rating">--}}
                      {{--<div class="ratings">--}}
                        {{--<div class="rating-box">--}}
                          {{--<div style="width:80%" class="rating"></div>--}}
                        {{--</div>--}}
                      {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="item-price">--}}
                      {{--<div class="price-box"> <span class="regular-price"> <span class="price">$235.00</span> </span> </div>--}}
                    {{--</div>--}}
                    {{--<div class="action"> </div>--}}
                  {{--</div>--}}
                {{--</div>--}}
              {{--</div>--}}
            {{--</div>--}}
          {{--</div>--}}
          {{--<div class="item">--}}
            {{--<div class="item-inner">--}}
              {{--<div class="item-img col-lg-4 col-md-4 col-sm-4 col-xs-12">--}}
                {{--<div class="item-img-info"> <a class="product_image image-wrapper" href="{!! route('product.detail', ['product_id' => $product->id, 'brand_id' => $product->brand_id]) !!}h"> <img class="front_image" alt="Pro Image" src="assets/shop/products-images/product3.jpg"></a> </div>--}}
              {{--</div>--}}
              {{--<div class="item-info col-lg-8 col-md-8 col-sm-8 col-xs-12">--}}
                {{--<div class="info-inner">--}}
                  {{--<div class="item-title"> <a title="sample" href="{!! route('product.detail', ['product_id' => $product->id, 'brand_id' => $product->brand_id]) !!}h"> Wing Chair </a> </div>--}}
                  {{--<div class="item-content">--}}
                    {{--<div class="rating">--}}
                      {{--<div class="ratings">--}}
                        {{--<div class="rating-box">--}}
                          {{--<div style="width:80%" class="rating"></div>--}}
                        {{--</div>--}}
                      {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="item-price">--}}
                      {{--<div class="price-box"> <span class="regular-price"> <span class="price">$235.00</span> </span> </div>--}}
                    {{--</div>--}}
                    {{--<div class="action"> </div>--}}
                  {{--</div>--}}
                {{--</div>--}}
              {{--</div>--}}
            {{--</div>--}}
          {{--</div>--}}
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="new-product">
          <div class="h2 products-section-title">BEST SELLER</div>
          @foreach($bestSellerProducts as $product)
          <div class="item">
            <div class="item-inner">
              <div class="item-img col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="item-img-info"> <a class="product_image image-wrapper" href="{!! route('product.detail', ['product_id' => $product->id, 'brand_id' => $product->brand_id]) !!}h"> <img class="back_image" alt="Pro Image" src="{{ URL::asset((json_decode($product->images))[0]) }}"></a> </div>
              </div>
              <div class="item-info col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div class="info-inner">
                  <div class="item-title"> <a title="sample" href="{!! route('product.detail', ['product_id' => $product->id, 'brand_id' => $product->brand_id]) !!}"> {{ $product->name }} </a> </div>
                  <div class="item-content">
                    <div class="rating">
                      <div class="ratings">
                        <div class="rating-box">
                          <div style="width:80%" class="rating"></div>
                        </div>
                      </div>
                    </div>
                    <div class="item-price">
                      <div class="price-box"> <span class="regular-price"> <span class="price">${{ $product->sell_price }}</span> </span> </div>
                    </div>
                    <div class="action"> </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="new-product">
          <div class="h2 products-section-title">Top Rate</div>
          @foreach($rateProducts as $product)
          <div class="item">
            <div class="item-inner">
              <div class="item-img col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="item-img-info"> <a class="product_image image-wrapper" href="{!! route('product.detail', ['product_id' => $product->id, 'brand_id' => $product->brand_id]) !!}h"> <img class="front_image" alt="Pro Image" src="{{ URL::asset((json_decode($product->images))[0]) }}"></a> </div>
              </div>
              <div class="item-info col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div class="info-inner">
                  <div class="item-title"> <a title="sample" href="{!! route('product.detail', ['product_id' => $product->id, 'brand_id' => $product->brand_id]) !!}h"> {{ $product->name }} </a> </div>
                  <div class="item-content">
                    <div class="rating">
                      <div class="ratings">
                        <div class="rating-box">
                          <div style="width:80%" class="rating"></div>
                        </div>
                      </div>
                    </div>
                    <div class="item-price">
                      <div class="price-box"> <span class="regular-price"> <span class="price">${{ $product->sell_price }}</span> </span> </div>
                    </div>
                    <div class="action"> </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
<!--3 Tabs end-->
<!-- Brand Logo -->
<div class="Faq-bar">
  <div class="container">
    <div class="row">
      <div class="h2 products-section-title">FAQ and Brands</div>
      <div id="testimonials" class="product-flexslider hidden-buttons col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="slider-items slider-width-col1 owl-carousel owl-theme">
          <div class="brand">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 brand-logo-size">
                <div class="brand1"> <img alt="logo" src="{{URL::to('assets/shop/images/b-logo1.png')}}"> </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 brand-logo-size">
                <div class="brand1"> <img alt="logo" src="{{URL::to('assets/shop/images/b-logo4.png')}}"> </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 brand-logo-size">
                <div class="brand1"> <img alt="logo" src="{{URL::to('assets/shop/images/b-logo5.png')}}" > </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 brand-logo-size">
                <div class="brand1"> <img alt="logo" src="{{URL::to('assets/shop/images/b-logo6.png')}}"></div>
              </div>
            </div>
          </div>
          <!-- Item -->
          <div class="brand">
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 brand-logo-size">
                <div class="brand1"> <img alt="logo" src="{{URL::to('assets/shop/images/b-logo1.png')}}"> </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 brand-logo-size">
                <div class="brand1"> <img alt="logo" src="{{URL::to('assets/shop/images/b-logo4.png')}}"> </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 brand-logo-size">
                <div class="brand1"> <img alt="logo" src="{{URL::to('assets/shop/images/b-logo6.png')}}" > </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 brand-logo-size">
                <div class="brand1"> <img alt="logo" src="{{URL::to('assets/shop/images/b-logo6.png')}}"></div>
              </div>
            </div>
          </div>
          <!-- End Item -->

          <!-- Item -->

        </div>
      </div>
      <div class="faq col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div id="faq-brand" class="panel-group">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title"><a href="#faq-collapse1" data-toggle="collapse" data-parent="#faq-brand"><em class="icon icon-anchor">&nbsp;</em>When are you open?</a></h4>
            </div>
            <div id="faq-collapse1" class="panel-collapse collapse in">
              <div class="panel-body">All our retails stores are open from 8.00 to 21.00 from Monday to Sunday.
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title"><a class="collapsed" href="#faq-collapse2" data-toggle="collapse" data-parent="#faq-brand"><em class="fa fa-bullhorn" aria-hidden="true">&nbsp;</em>Is there an Furniture store near me?</a></h4>
            </div>
            <div id="faq-collapse2" class="panel-collapse collapse">
              <div class="panel-body">Locate your local store here.
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title"><a class="collapsed" href="#faq-collapse3" data-toggle="collapse" data-parent="#faq-brand"><em class="icon icon-anchor">&nbsp;</em>Why are some items sometimes out of stock?</a></h4>
            </div>
            <div id="faq-collapse3" class="panel-collapse collapse">
              <div class="panel-body">We strive to maintain availability of all items listed in our catalog. However, due to the success of a particular item or supply delays, it is possible that an item can be temporarily out of stock. Most times, our store co-workers can provide delivery information on an out of stock item. Furniture products are manufactured all over the world and there are many circumstances that can sometimes impact supply.
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title"><a class="collapsed" href="#faq-collapse4" data-toggle="collapse" data-parent="#faq-brand"><em class="icon icon-refresh">&nbsp;</em>Can you pay for an item online and collect in store?</a></h4>
            </div>
            <div id="faq-collapse4" class="panel-collapse collapse">
              <div class="panel-body">Furniture shop does not currently offer this service.
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end Brand Logo -->
@endsection
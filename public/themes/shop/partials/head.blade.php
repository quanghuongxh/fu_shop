<meta http-equiv="x-ua-compatible" content="ie=edge">
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>@yield('title')</title>

<!-- Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS Style -->
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/font-awesome.css')}}" media="all">
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/simple-line-icons.css')}}" media="all">
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/owl.carousel.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/owl.theme.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/jquery.bxslider.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/jquery.mobile-menu.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/revslider.css')}}">
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/style.css')}}" media="all">
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/blog.css')}}" media="all">
<link rel="stylesheet" type="text/css" href="{{URL::to('assets/shop/css/flexslider.css')}}" media="all">

<!-- Google Fonts -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,600,500,700,800' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">

<style type="text/css">
	.help-block{
		color: red;
	}
	#livechat-eye-catcher{
		display: none;
	}
</style>
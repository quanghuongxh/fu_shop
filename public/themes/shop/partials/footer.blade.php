<!-- Footer -->
<footer id="footer">
  <div class="footer-container">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
          <div class="block-contact">
            <ul id="block-contact_list">
              <li class="block_1"> <i class="fa fa-map-marker"></i> <span class="contactdiv">Park Row, London SE10 9LS, UK</span> </li>
              <li class="block_2"> <i class="fa fa-phone"></i> <span>+44 20 8331 9000</span> </li>
              <li class="block_3"> <i class="fa fa-envelope"></i> <span>sales@fushop.vn</span> </li>
            </ul>
          </div>
        </div>
        <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
          <div class="block_newsletter links">
            <div class="newsletter_bg">
              <h2 class="h1 newsletter">Newsletter</h2>
              <div class="collapse">
                <div class="title-text"> To receive latest offers and discounts from the shop. </div>
                <form id="subscribe-form" method="post" action="#">
                  <div class="subscribe-content">
                    <input type="text" name="subscribe-input" id="subscribe-input" value="" placeholder="Enter Your Email Address" class="form-control input-text required-entry validate-email">
                    <button class="button" type="submit"><span>Subscribe</span></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 links wrapper">
              <h3> <a href="#" rel="nofollow"> Your account </a> </h3>
              <ul class="account-list collapse" id="footer_account_list">
                <li> <a href="#" title="Addresses" rel="nofollow"> Addresses </a> </li>
                <li> <a href="#" title="Credit slips" rel="nofollow"> Credit slips </a> </li>
                <li> <a href="#" title="Orders" rel="nofollow"> Orders </a> </li>
                <li> <a href="#" title="Personal info" rel="nofollow"> Personal info </a> </li>
              </ul>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 links block links">
              <h3>Products</h3>
              <ul class="collapse block_content">
                <li> <a id="link-cms-page-1-1" class="cms-page-link" href="#" title="Our terms and conditions of delivery"> Delivery </a> </li>
                <li> <a id="link-cms-page-2-1" class="cms-page-link" href="#" title="Legal notice"> Legal Notice </a> </li>
                <li> <a id="link-cms-page-4-1" class="cms-page-link" href="#" title="Learn more about us"> About us </a> </li>
                <li> <a id="link-product-page-prices-drop-1" class="cms-page-link" href="#" title="Our special products"> Prices drop </a> </li>
                <li> <a id="link-product-page-new-products-1" class="cms-page-link" href="#" title="Our new products"> New products </a> </li>
                <li> <a id="link-product-page-best-sales-1" class="cms-page-link" href="#" title="Our best sales"> Best sales </a> </li>
              </ul>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 links block links">
              <h3>Our company</h3>
              <ul class="collapse block_content">
                <li> <a id="link-cms-page-1-2" class="cms-page-link" href="#" title="Our terms and conditions of delivery"> Delivery </a> </li>
                <li> <a id="link-cms-page-2-2" class="cms-page-link" href="#" title="Legal notice"> Legal Notice </a> </li>
                <li> <a id="link-cms-page-4-2" class="cms-page-link" href="#" title="Learn more about us"> About us </a> </li>
                <li> <a id="link-cms-page-5-2" class="cms-page-link" href="#" title="Our secure payment method"> Secure payment </a> </li>
                <li> <a id="link-static-page-contact-2" class="cms-page-link" href="#" title="Use our form to contact us"> Contact us </a> </li>
                <li> <a id="link-static-page-sitemap-2" class="cms-page-link" href="#" title="Lost ? Find what your are looking for"> Sitemap </a> </li>
                <li> <a id="link-static-page-stores-2" class="cms-page-link" href="#" title=""> Stores </a> </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="container">
        <div class="row"> 
          <!-- Block links module -->
          <div id="links_block_left" class="block col-md-4 links">
            <ul id="tm_blocklink" class="block_content collapse">
              <li> <a href="#" title="about us">About Us</a></li>
              <li> <a href="#" title="business">Business</a></li>
              <li> <a href="#" title="featured products">Featured Products</a></li>
              <li> <a href="#" title="new products">New Products</a></li>
              <li> <a href="#" title="decorative lighting">Decorative Lighting</a></li>
              <li> <a href="#" title="storage and covers">Storage and Covers</a></li>
            </ul>
          </div>
          <!-- /Block links module --> 
        </div>
      </div>
      <div class="copyright"> <a class="_blank" href="#" target="_blank"> &copy; 2017 HappyStudio. Design By Dinh Hoang. </a> </div>
    </div>
  </div>
</footer>

<!-- End Footer --> 

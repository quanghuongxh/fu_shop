<div id="page">
    <!-- Header -->
    <header>
        <div class="header-container">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12 logo-block">
                        <!-- Header Logo -->
                        <div class="logo"> <a title="Magento Commerce" href="{!! route('index') !!}"><img alt="Magento Commerce" src="{{URL::to('assets/shop/images/logo.png')}}"> </a> </div>
                        <!-- End Header Logo -->
                    </div>
                    <div class="col-lg-8 col-md-9 col-sm-9 col-xs-12 thm-top-cart">
                        <div class="cart-account">
                            <div class="dropdown login">
                                <div class="user-circle"> <a role="button" data-toggle="dropdown" data-target="#" class="dropdown-toggle" href="#"> <i class="fa fa-user" aria-hidden="true"></i></a>
                                    <ul class="user-info dropdown-menu" role="menu">
                                        @if(!Session::has('user_id'))
                                        <li><a href="{{ route('customer.login')}}" title="Log into your customer account" rel="nofollow"> <span class="hidden-md-down">Login</span> </a> / <a href="{{ route('customer.register')}}" title="Register new account" rel="nofollow"> <span class="hidden-md-down">Register</span> </a> </li>
                                        @else
                                        <li><a href="{{ route('cus.order')}}" title="My orders" rel="nofollow"> <span class="hidden-md-down">My-orders</span> </a> / <a href="{{ route('cus.profile')}}" title="Display your information" rel="nofollow"> <span class="hidden-md-down">Profile</span> </a>  / <a href="{{ route('user.logout')}}" title="Log out" rel="nofollow"> <span class="hidden-md-down">Logout</span> </a></li>
                                        @endif
                                        <li class="language-selector-wrapper">
                                            <div class="current"> <span class="cur-label">Language</span> </div>
                                            <div class="language-selector dropdown js-dropdown">
                                                <ul class="dropdown-menu hidden-md-down languages-block_ul">
                                                    <li class="current"> <a href="#" class="dropdown-item"><img alt="Flag" src="{{URL::to('assets/shop/images/1.jpg')}}"></a> </li>
                                                    <li> <a href="#" class="dropdown-item"><img alt="Flag" src="{{URL::to('assets/shop/images/6.png')}}"></a> </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="top-cart-contain pull-right">
                                <!-- Top Cart -->
                                <div class="mini-cart">
                                    <div data-toggle="dropdown" data-hover="dropdown" class="basket dropdown-toggle"> <a href="#"> <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="badge" style="margin-left: 4em; margin-top: -5em;">{{ Session::has('cart') ? Session::get('cart')->totalQty : '' }}</span></a> </div>
                                    <div>
                                        <div class="top-cart-content">
                                            <!--block-subtitle-->
                                            <ul class="mini-products-list" id="cart-sidebar">
                                                {{--@if(Session::has('cart'))--}} {{--
                                                <li class="item first">--}} {{--
                                                    <div class="item-inner"> <a class="product-image" title="products" href="#l"><img alt="products" src="{{Session::get('cart')->totalQty}}"> </a>--}} {{--
                                                        <div class="product-details">--}} {{--
                                                            <div class="access"><a class="btn-remove1" title="Remove This Item" href="#">Remove</a> <a class="btn-edit" title="Edit item" href="#"><i class="icon-pencil"></i><span class="hidden">Edit item</span></a> </div>--}} {{--
                                                            <!--access--><strong>1</strong> x <span class="price">$179.99</span>--}} {{--
                                                            <p class="product-name"><a href="#">Study Table...</a> </p>--}} {{--
                                                        </div>--}} {{--
                                                    </div>--}} {{--
                                                </li>--}} {{--@elseif--}} {{--@endif--}} {{--
                                                <li class="item last">--}} {{--
                                                    <div class="item-inner"> <a class="product-image" title="Epson L360 Printer" href="#"><img alt="Epson L360 Printer" src="{{URL::to('products-assets/shop/images/product3.jpg')}}"> </a>--}} {{--
                                                        <div class="product-details">--}} {{--
                                                            <div class="access"><a class="btn-remove1" title="Remove This Item" href="#">Remove</a> <a class="btn-edit" title="Edit item" href="#"><i class="icon-pencil"></i><span class="hidden">Edit item</span></a> </div>--}} {{--
                                                            <!--access--><strong>1</strong> x <span class="price">$80.00</span>--}} {{--
                                                            <p class="product-name"><a href="#">Arm Chair...</a> </p>--}} {{--
                                                        </div>--}} {{--
                                                    </div>--}} {{--
                                                </li>--}}
                                            </ul>
                                            <!--actions-->
                                            <div class="actions">
                                                <a href="{!! route('checkout') !!}" class="btn-checkout" title="Checkout" type="button" style="padding-right: 1.5em;"><span>Checkout</span> </a>
                                                <a href="{!! route('product.cart') !!}" class="view-cart"><span>View Cart</span></a> </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Top Cart -->
                                <div id="ajaxconfig_info" style="display:none"> <a href="#/"></a>
                                    <input value="" type="hidden">
                                    <input id="enable_module" value="1" type="hidden">
                                    <input class="effect_to_cart" value="1" type="hidden">
                                    <input class="title_shopping_cart" value="Go to shopping cart" type="hidden">
                                </div>
                            </div>
                        </div>
                        <div class="care tmservicecms">
                            <div class="text1">Customer care</div>
                            <div class="text2">call us:- +84-962-289-6017</div>
                        </div>
                        <div class="chat tmservicecms">
                            <div class="text1">Email us</div>
                            <a href="mailto:help@fushop.com" id="email_footer" style="color: #808080">help@fushop.com</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- end header -->
    <!-- Navigation -->
    <nav class="">
        <div class="container">
            <div class="mm-toggle-wrap">
                <div class="mm-toggle"><i class="fa fa-bars"></i><span class="mm-label">Menu</span> </div>
            </div>
            <div class="nav-inner">
                <!-- BEGIN NAV -->
                <ul id="nav" class="hidden-xs">
                    <li class="level0 parent drop-menu active" id="nav-home"><a href="{{ route('index') }}" class="level-top"><span>Home</span></a> </li>
                    <!-- <li class="level0 nav-6 level-top drop-menu"> <a class="level-top" href="#"> <span>Pages</span> </a>
                        <ul class="level1">
                            <li class="level2 first"><a href="#"><span>Left Sidebar</span></a> </li>
                            <li class="level2 first"><a href="#"><span>Right Sidebar</span></a> </li>
                            <li class="level2 first"><a href="#"><span>Full Width</span></a> </li>
                            <li class="level2 nav-10-2"> <a href="#"> <span>List</span> </a> </li>
                            <li class="level2 nav-10-3"> <a href="#"> <span>Product Detail</span> </a> </li>
                            <li class="level2 nav-10-4"> <a href="#"> <span>Shopping Cart</span> </a> </li>
                            <li class="level2 parent"><a href="#"><span>Checkout</span></a> </li>
                            <li class="level1 nav-10-4"> <a href="#"> <span>Wishlist</span> </a> </li>
                            <li class="level1"> <a href="#"> <span>Dashboard</span> </a> </li>
                            <li class="level1"> <a href="#"> <span>Multiple Addresses</span> </a> </li>
                            <li class="level1"><a href="quick_view.html"><span>Quick view</span></a> </li>
                            <li class="level1"><a href="#"><span>Newsletter</span></a> </li>
                            <li class="level1"><a href="404error.html"><span>404 Error Page</span></a> </li>
                            <li class="level1"><a href="#"><span>About us</span></a> </li>
                            <li class="level1"><a href="#"><span>Blog</span></a> </li>
                            <li class="level1"><a href="#"><span>Blog Detail</span></a> </li>
                            <li class="level1"><a href="#"><span>Contact us</span></a> </li>
                        </ul>
                    </li> -->
                    <li class="mega-menu"> <a class="level-top" href="#"><span>Living Room</span></a>
                        <div class="level0-wrapper dropdown-6col" style="display: none; left: 0px;">
                            <div class="container">
                                <div class="level0-wrapper2">
                                    <div class="nav-block nav-block-center">
                                        <div class="col-1">
                                            <ul class="level0">
                                                <li class="level1 nav-6-1 parent item"> <a href="#"><span>Sofas</span></a>
                                                    <ul class="level1">
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Sofa Seater</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Sofa bed</span></a></li>
                                                    </ul>
                                                </li>
                                                <li class="level1 nav-6-1 parent item"> <a href="#"><span>Chairs</span></a>
                                                    <ul class="level1">
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Recliner Chairs</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Arm Chairs</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Rocking Chairs</span></a></li>
                                                    </ul>
                                                </li>
                                                <li class="level1 nav-6-1 parent item"> <a href="#"><span>Tables</span></a>
                                                    <ul class="level1">
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Basic</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Rectangle Shape</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Round shape</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Other Tables</span></a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- level -->
                                        <!-- Right Menu images -->
                                        <div class="col-2">
                                            <div class="menu_image1"> <a title="" href="#"><img alt="menu_image" src="{{URL::to('assets/shop/images/menu-img.jpg')}}"></a> </div>
                                        </div>
                                    </div>
                                    <!--nav-block nav-block-center-->
                                </div>
                                <!-- level0-wrapper2 -->
                            </div>
                            <!-- container -->
                        </div>
                        <!--level0-wrapper dropdown-6col-->
                        <!--mega menu-->
                        <!--mega menu-->
                        <li class="mega-menu"> <a class="level-top" href="#"><span>Dinning Room</span></a>
                            <div class="level0-wrapper dropdown-6col">
                                <div class="container">
                                    <div class="level0-wrapper2">
                                        <!---   <div class="nav-add"> <a href="#"><img src="assets/shop/images/menu-banner.jpg" alt="download"> </a> </div> -->
                                        <div class="nav-block nav-block-center">
                                            <!--mega menu-->
                                            <ul class="level0">
                                                <li class="level3 nav-6-1 parent item"> <a href="#"><span>Dinning Sets</span></a>
                                                    <ul class="level1">
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Eight Seater</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Four Seater</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Six Seater</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Two Seater</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Sixteen Seater</span></a></li>
                                                        <li class="menu-img-block"><img alt="menu_image" src="{{URL::to('assets/shop/images/menu-img1.jpg')}}"></li>
                                                    </ul>
                                                </li>
                                                <li class="level3 nav-6-1 parent item"> <a href="#"><span>Dinning Tables</span></a>
                                                    <ul class="level1">
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Colonial</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Contemporary</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Eclectic</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Modern</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Old</span></a></li>
                                                        <li class="menu-img-block"><img alt="menu_image" src="{{URL::to('assets/shop/images/menu-img2.jpg')}}"></li>
                                                    </ul>
                                                </li>
                                                <li class="level3 nav-6-1 parent item"> <a href="#"><span>Dinning Chairs</span></a>
                                                    <ul class="level1">
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Colonial</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Contemporary</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Eclectic</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Modern</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Old</span></a></li>
                                                        <li class="menu-img-block"><img alt="menu_image" src="{{URL::to('assets/shop/images/menu-img3.jpg')}}"></li>
                                                    </ul>
                                                </li>
                                                <li class="level3 nav-6-1 parent item"> <a href="#"><span>Sideboards</span></a>
                                                    <ul class="level1">
                                                        <li class="level2 nav-6-1-1"> <a href="#"><span>Cabinets</span></a> </li>
                                                        <li class="level2 nav-6-1-1"> <a href="#"><span>Drawers</span></a> </li>
                                                        <li class="level2 nav-6-1-1"> <a href="#"><span>Glass</span></a> </li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Metal</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Modern</span></a></li>
                                                        <li class="menu-img-block"><img alt="menu_image" src="{{URL::to('assets/shop/images/menu-img3.jpg')}}"></li>
                                                    </ul>
                                                </li>
                                                <li class="level3 nav-6-1 parent item"> <a href="#"><span>Bar Stools</span></a>
                                                    <ul class="level1">
                                                        <li class="level2 nav-6-1-1"> <a href="#"><span>Adjustable</span></a> </li>
                                                        <li class="level2 nav-6-1-1"> <a href="#"><span>Bar Height</span></a> </li>
                                                        <li class="level2 nav-6-1-1"> <a href="#"><span>Counter Height</span></a> </li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Swivel</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Short</span></a></li>
                                                        <li class="menu-img-block"><img alt="menu_image" src="{{URL::to('assets/shop/images/menu-img5.jpg')}}"></li>
                                                    </ul>
                                                </li>
                                                <li class="level3 nav-6-1 parent item"> <a href="#"><span>Serving Carts</span></a>
                                                    <ul class="level1">
                                                        <li class="level2 nav-6-1-1"> <a href="#"><span>Glass</span></a> </li>
                                                        <li class="level2 nav-6-1-1"> <a href="#"><span>Steel</span></a> </li>
                                                        <li class="level2 nav-6-1-1"> <a href="#"><span>Traditional</span></a> </li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Wooden</span></a></li>
                                                        <li class="level2 nav-6-1-1"><a href="#"><span>Modern</span></a></li>
                                                        <li class="menu-img-block"><img alt="menu_image" src="{{URL::to('assets/shop/images/menu-img6.jpg')}}"></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                            <!--level0-->
                                        </div>
                                        <!--nav-block nav-block-center-->
                                    </div>
                                    <!--level0-wrapper2-->
                                    <!--container-->
                                </div>
                            </div>
                        </li>
                        <li class="level0 nav-6 level-top drop-menu"> <a class="level-top" href="#"> <span>BedRoom</span> </a>
                            <ul class="level1">
                                <li class="level2 first"><a href="#"><span>Beds</span></a> </li>
                                <li class="level2 nav-10-2"> <a href="#"> <span>Dressing Tables</span> </a> </li>
                                <li class="level2 nav-10-3"> <a href="#"> <span>Wardrobes</span> </a> </li>
                                <li class="level2 nav-10-4"> <a href="#"> <span>Mattresses</span> </a> </li>
                                <li class="level2 nav-10-3"> <a href="#"> <span>Bedside Tables</span> </a> </li>
                                <li class="level2 nav-10-4"> <a href="#"> <span>Nightstands</span> </a> </li>
                            </ul>
                        </li>
                        <li class="mega-menu"> <a class="level-top" href="#"><span>Kitchen</span></a> </li>
                </ul>
                <!--nav-->
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 hidden-xs search-block">
                    <div class="search-box">
                        <form action="cat" method="POST" id="search_mini_form" name="Categories">
                            <input type="text" placeholder="Search entire store here..." value="what are you looking for... " maxlength="70" name="search" id="search">
                            <button type="button" class="search-btn-bg"><span class="glyphicon glyphicon-search"></span>&nbsp;</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <!-- end nav -->
    <!-- mobile menu -->
    <div id="mobile-menu">
        <ul>
            <li>
                <div class="mm-search">
                    <form id="search1" name="search">
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i> </button>
                            </div>
                            <input type="text" class="form-control simple" placeholder="Search ..." name="srch-term" id="srch-term">
                        </div>
                    </form>
                </div>
            </li>
            <li><a href="#">Home</a> </li>
            <li><a href="#">Pages</a>
                <ul>
                    <li><a href="#">Left Sidebar</a> </li>
                    <li><a href="#">Right Sidebar</a> </li>
                    <li><a href="#">Full Width</a> </li>
                    <li> <a href="#">List</a> </li>
                    <li> <a href="#">Product Detail</a> </li>
                    <li> <a href="#">Shopping Cart</a> </li>
                    <li><a href="#">Checkout</a> </li>
                    <li> <a href="#">Wishlist</a> </li>
                    <li> <a href="#">Dashboard</a> </li>
                    <li> <a href="#">Multiple Addresses</a> </li>
                    <li> <a href="#">About us</a> </li>
                    <li><a href="#">Blog</a>
                        <ul>
                            <li><a href="#">Blog Detail</a> </li>
                        </ul>
                    </li>
                    <li><a href="#">Contact us</a> </li>
                    <li><a href="#">404 Error Page</a> </li>
                </ul>
            </li>
            <li><a href="#">Living Room</a>
                <ul>
                    <li> <a href="#" class="">Bean Bags</a>
                        <ul>
                            <li> <a href="#" class="">Elongated</a> </li>
                            <li> <a href="#" class="">Novelty</a> </li>
                            <li> <a href="#" class="">Round</a> </li>
                            <li> <a href="#" class="">Square</a> </li>
                        </ul>
                    </li>
                    <li> <a href="#">Chairs</a>
                        <ul>
                            <li> <a href="#">Arm Chairs</a> </li>
                            <li> <a href="#">Folding Chairs</a> </li>
                            <li> <a href="#">Metal Chairs</a> </li>
                            <li> <a href="#">Recliner</a> </li>
                        </ul>
                    </li>
                    <li> <a href="#">Racks</a>
                        <ul>
                            <li> <a href="#">Diaplay Unit</a> </li>
                            <li> <a href="#">Magazine Rack</a> </li>
                            <li> <a href="#">Shoe Rack</a> </li>
                            <li> <a href="#">Trunks & Boxes</a> </li>
                        </ul>
                    </li>
                    <li> <a href="#">Sofas</a>
                        <ul>
                            <li> <a href="#">Corner Sofas</a> </li>
                            <li> <a href="#">Leather Sofas</a> </li>
                            <li> <a href="#">Sofa Beds</a> </li>
                            <li> <a href="#">Sofa Suites</a> </li>
                        </ul>
                    </li>
                    <li> <a href="#">Stools</a>
                        <ul>
                            <li> <a href="#">Designer</a> </li>
                            <li> <a href="#">Folding</a> </li>
                            <li> <a href="#">Iron</a> </li>
                            <li> <a href="#">Wood</a> </li>
                        </ul>
                    </li>
                    <li> <a href="#">Tables</a>
                        <ul>
                            <li> <a href="#">Coffee Tables</a> </li>
                            <li> <a href="#">Console Tables</a> </li>
                            <li> <a href="#">Set of Tables</a> </li>
                            <li> <a href="#">Side Tables</a> </li>
                        </ul>
                    </li>
                    <li> <a href="#">Textiles</a>
                        <ul>
                            <li> <a href="#">Curtain Rods</a> </li>
                            <li> <a href="#">Cushions</a> </li>
                            <li> <a href="#">Fabrics</a> </li>
                            <li> <a href="#">Benches</a> </li>
                        </ul>
                    </li>
                    <li> <a href="#">Storage</a>
                        <ul>
                            <li> <a href="#">Buffets</a> </li>
                            <li> <a href="#">Cabinets</a> </li>
                            <li> <a href="#">Sideboards</a> </li>
                            <li> <a href="#">Bookcases</a> </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="#">Dinning Room</a>
                <ul>
                    <li> <a href="#" class="">Dinning Sets </a>
                        <ul class="level1">
                            <li class="level2 nav-6-1-1"><a href="#">Eight Seater</a> </li>
                            <li class="level2 nav-6-1-1"><a href="#">Four Seater</a> </li>
                            <li class="level2 nav-6-1-1"><a href="#">Six Seater</a> </li>
                            <li class="level2 nav-6-1-1"><a href="#">Two Seater</a> </li>
                        </ul>
                    </li>
                    <li> <a href="#">Dinning Tables</a>
                        <ul class="level1">
                            <li class="level2 nav-6-1-1"> <a href="#">Colonial</a> </li>
                            <li class="level2 nav-6-1-1"> <a href="#">Contemporary</a> </li>
                            <li class="level2 nav-6-1-1"> <a href="#">Eclectic</a> </li>
                            <li class="level2 nav-6-1-1"> <a href="#">Modern</a> </li>
                        </ul>
                    </li>
                    <li> <a href="#">Dinning Chairs</a>
                        <ul class="level1">
                            <li class="level2 nav-6-1-1"> <a href="#">Colonial</a> </li>
                            <li class="level2 nav-6-1-1"> <a href="#">Contemporary</a> </li>
                            <li class="level2 nav-6-1-1"> <a href="#">Eclectic</a> </li>
                            <li class="level2 nav-6-1-1"> <a href="#">Modern</a> </li>
                        </ul>
                    </li>
                    <li> <a href="#">Sideboards</a>
                        <ul class="level1">
                            <li class="level2 nav-6-1-1"> <a href="#">Cabinets</a> </li>
                            <li class="level2 nav-6-1-1"> <a href="#">Drawers</a> </li>
                            <li class="level2 nav-6-1-1"> <a href="#">Glass</a> </li>
                            <li class="level2 nav-6-1-1"> <a href="#">Modern</a> </li>
                        </ul>
                    </li>
                    <li> <a href="#">Bar Stools</a>
                        <ul class="level1">
                            <li class="level2 nav-6-1-1"> <a href="#">Adjustable</a> </li>
                            <li class="level2 nav-6-1-1"> <a href="#">Bar Height</a> </li>
                            <li class="level2 nav-6-1-1"> <a href="#">Counter Height</a> </li>
                            <li class="level2 nav-6-1-1"> <a href="#">Swivel</a> </li>
                        </ul>
                    </li>
                    <li> <a href="#">Serving Carts</a>
                        <ul class="level1">
                            <li class="level2 nav-6-1-1"> <a href="#">Glass</a> </li>
                            <li class="level2 nav-6-1-1"> <a href="#">Steel</a> </li>
                            <li class="level2 nav-6-1-1"> <a href="#">Traditional</a> </li>
                            <li class="level2 nav-6-1-1"> <a href="#">Modern</a> </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="#">BedRoom</a>
                <ul>
                    <li> <a href="#">Beds</a> </li>
                    <li> <a href="#">Dressing Tables</a> </li>
                    <li> <a href="#">Wardrobes</a> </li>
                    <li> <a href="#">Mattresses</a> </li>
                    <li> <a href="#">Bedside Tables</a> </li>
                    <li> <a href="#">Nightstands</a> </li>
                </ul>
            </li>
            <li><a href="#">Kitchen</a> </li>
            <li><a href="#">Blog</a> </li>
            <li><a href="#">Custom</a> </li>
        </ul>
        <div class="top-links">
            <ul class="links">
                <li><a title="My Account" href="#">My Account</a> </li>
                <li><a title="Wishlist" href="#">Wishlist</a> </li>
                <li><a title="Checkout" href="#">Checkout</a> </li>
                <li><a title="Blog" href="#">Blog</a> </li>
                <li class="last"><a title="Login" href="#">Login</a> </li>
            </ul>
        </div>
    </div>
<script type="text/javascript" src="{{URL::to('assets/shop/js/jquery.min.js')}}"></script> 
<script type="text/javascript" src="{{URL::to('assets/shop/js/bootstrap.min.js')}}"></script> 
<script type="text/javascript" src="{{URL::to('assets/shop/js/revslider.js')}}"></script> 
<script type="text/javascript" src="{{URL::to('assets/shop/js/common.js')}}"></script> 
<script type="text/javascript" src="{{URL::to('assets/shop/js/owl.carousel.min.js')}}"></script> 
<script type="text/javascript" src="{{URL::to('assets/shop/js/jquery.mobile-menu.min.js')}}"></script> 
<script type="text/javascript" src="{{URL::to('assets/shop/js/countdown.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/shop/js/jquery.flexslider.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/shop/js/cloud-zoom.js')}}"></script>
<script type='text/javascript'>
	jQuery(document).ready(function() {
		jQuery('#rev_slider_4').show().revolution({
			dottedOverlay: 'none',
			delay: 5000,
			startwidth:1170,
			startheight: 684,
			hideThumbs: 200,
			thumbWidth: 200,
			thumbHeight: 50,
			thumbAmount: 2,
			navigationType: 'thumb',
			navigationArrows: 'solo',
			navigationStyle: 'round',
			touchenabled: 'on',
			onHoverStop: 'on',
			swipe_velocity: 0.7,
			swipe_min_touches: 1,
			swipe_max_touches: 1,
			drag_block_vertical: false,
			spinner: 'spinner0',
			keyboardNavigation: 'off',
			navigationHAlign: 'center',
			navigationVAlign: 'bottom',
			navigationHOffset: 0,
			navigationVOffset: 20,
			soloArrowLeftHalign: 'left',
			soloArrowLeftValign: 'center',
			soloArrowLeftHOffset: 20,
			soloArrowLeftVOffset: 0,
			soloArrowRightHalign: 'right',
			soloArrowRightValign: 'center',
			soloArrowRightHOffset: 20,
			soloArrowRightVOffset: 0,
			shadow: 0,
			fullWidth: 'on',
			fullScreen: 'off',
			stopLoop: 'off',
			stopAfterLoops: -1,
			stopAtSlide: -1,
			shuffle: 'off',
			autoHeight: 'off',
			forceFullWidth: 'on',
			fullScreenAlignForce: 'on',
			minFullScreenHeight: 0,
			hideNavDelayOnMobile: 1500,
			hideThumbsOnMobile: 'off',
			hideBulletsOnMobile: 'off',
			hideArrowsOnMobile: 'off',
			hideThumbsUnderResolution: 0,
			hideSliderAtLimit: 0,
			hideCaptionAtLimit: 0,
			hideAllCaptionAtLilmit: 0,
			startWithSlide: 0,
			fullScreenOffsetContainer: ''
		});
	});
</script> 

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 9248680;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of LiveChat code -->
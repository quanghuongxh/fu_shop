@extends('shop.layout.app')

@section('title')
Furniture Shopping Cart
@endsection

@section('content')
<section class="main-container col1-layout">
  <div class="main container">
    <div class="col-main">
      @if(Session::has('cart'))
      <div class="cart wow bounceInUp animated">
        <div class="page-title">
          <h2>Shopping Cart</h2>
        </div>
        <div class="table-responsive">

          <input type="hidden" value="Vwww7itR3zQFe86m" name="form_key">
          <fieldset>
            <table class="data-table cart-table" id="shopping-cart-table">
              <colgroup>
                <col width="1">
                <col>
                <col width="1">
                <col width="1">
                <col width="1">
                <col width="1">
                <col width="1">
              </colgroup>
              <thead>
                <tr class="first last">
                  <th rowspan="1">&nbsp;</th>
                  <th rowspan="1"><span class="nobr">Product Name</span></th>
                  <th rowspan="1"></th>
                  <th colspan="1" class="a-center"><span class="nobr">Unit Price</span></th>
                  <th class="a-center" rowspan="1">Qty</th>
                  <th colspan="1" class="a-center">Subtotal</th>
                  <th class="a-center" rowspan="1">&nbsp;</th>
                </tr>
              </thead>
              <tfoot>
                <tr class="first last">
                  <td class="a-right last" colspan="50">
                    <a href="{!! route('index') !!}" class="button btn-continue" title="Continue Shopping"type="button">
                      <span>Continue Shopping</span>
                    </a>
                 <button id="empty_cart_button" class="button btn-empty" title="Clear Cart"
                 value="empty_cart">
                 <span>Clear Cart</span>
               </button>
             </td>
           </tr>
         </tfoot>
         <tbody>
          @foreach($products as $product)
          <tr class="first">
            <td class="image">
              <a class="product-image" title="product lapen casen"
              href="{!! route('product.detail', ['product_id' => $product['product']['id'], 'brand_id' => $product['product']['brand_id']]) !!}">
              <img src="{{ URL::asset(json_decode($product['product']['images'])[0]) }}" alt="" width="75">
            </a>
          </td>
          <td><h2 class="product-name"><a
            href="{!! route('product.detail', ['product_id' => $product['product']['id'], 'brand_id' => $product['product']['brand_id']]) !!}">{{ $product['product']['name'] }}</a>
          </h2></td>
          <td class="a-center"><a title="Edit item parameters" class="edit-bnt"
            href="#configure/id/15945/"></a></td>
            <td class="a-right"><span class="cart-price"> <span
              class="price">${{ $product['product']['sell_price'] }}</span></span>
            </td>
            <td class="a-center movewishlist">
              <select class="quantity" data-id="{{ $product['product']['id'] }}">
                <option {{  $product['qty'] == 1 ? 'selected' : '' }}>1</option>
                <option {{  $product['qty'] == 2 ? 'selected' : '' }}>2</option>
                <option {{  $product['qty'] == 3 ? 'selected' : '' }}>3</option>
                <option {{  $product['qty'] == 4 ? 'selected' : '' }}>4</option>
                <option {{  $product['qty'] == 5 ? 'selected' : '' }}>5</option>
              </select>
            </td>
            <td class="a-right movewishlist"><span class="cart-price"> <span
              class="subprice">${{ $product['price']}}</span> </span></td>
              <td class="a-center last"><a class="button remove-item" title="Remove item"
               href="{{ route('cart.removeItem',['id' => $product['product']['id'] ]) }}"><span><span>Remove item</span></span></a>
             </td>
           </tr>
           @endforeach
         </tbody>
       </table>
     </fieldset>

   </div>
   <!-- BEGIN CART COLLATERALS -->
   <div class="cart-collaterals row">
    <div class="col-sm-4">
      <div class="discount">
        <h3>Discount Codes</h3>
        <form method="post" action="#couponPost/" id="discount-coupon-form">
          <label for="coupon_code">Enter your coupon code if you have one.</label>
          <input type="hidden" value="0" id="remove-coupone" name="remove">
          <input type="text" value="" name="coupon_code" id="coupon_code"
          class="input-text fullwidth">
          <button class="button coupon" title="Apply Coupon" type="button" disabled><span>Apply Coupon</span>
          </button>
        </form>
      </div>
    </div>
    <div class="col-sm-4">

    </div>
    <div class="col-sm-4">
      <div class="totals">
        <h3>Shopping Cart Total</h3>
        <div class="inner">
          <table class="table shopping-cart-table-total" id="shopping-cart-totals-table">
            <colgroup>
              <col>
              <col width="1">
            </colgroup>
            <tbody>
              <tr>
                <td colspan="1" class="a-left" style="">Subtotal</td>
                <td class="a-right" style=""><span class="price">$ {{ $totalPrice }}</span></td>
              </tr>
              <tr>
                <td colspan="1" class="a-left" style="">Shipping</td>
                <td class="a-right" style=""><span class="price">Free</span></td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="1" class="a-left" style=""><strong>Total</strong></td>
                <td class="a-right" style=""><strong><span
                  class="price total_price">$ {{ $totalPrice }}</span></strong></td>
                </tr>
              </tfoot>
            </table>
            <ul class="checkout">
              <li>
                <a href="{!! route('checkout') !!}" class="button btn-proceed-checkout"
                title="Proceed to Checkout" type="button"
                style="text-decoration: none; display: inline-block;padding: 7px 17px;border-radius: 999px;margin-right: 8px;font-size: 12px;text-transform: uppercase;font-weight: 700;letter-spacing: 0.5px;cursor: pointer;line-height: inherit;"><span>Proceed to Checkout</span></a>
              </li>
              <br>
            </ul>
          </div>
          <!--inner-->
        </div>
      </div>
    </div>

    <!--cart-collaterals-->

  </div>
  @else
  <div class="row">
    <div class="col-md-12"><a class="continue-shopping"
      style="padding:2em;background: url({{ URL::to('assets/shop/images/continue-shopping.png')}}) no-repeat;height: 23px;display: block;margin-top: 33px;font-family: Lato;font-size: 14px;font-weight: bold;letter-spacing: 2px;line-height: 1.3em;text-decoration: none;text-transform: uppercase;color: #58585C;"
      href="{{ route('index') }}"></a></div>
    </div>
    <h3 align="center">
      No products in cart!
    </h3>
    @endif
    <div id="result">
    </div>

  </div>
</div>
</section>

@endsection
@section('script')
<script>
  $(document).ready(function () {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $('.btn-empty').click(function() {
      $.ajax({
        type: 'GET',
        url: '/shop/cart/destroy',
        data:{},
        success: function (data) {
          location.reload();
        },
        error: function (data) {
          console.log('Error:', data);
        }
      });
    });
    $('.quantity').on('change', function () {
      var id = $(this).attr('data-id');
      $.ajax({
        type: 'POST',
        url: '/shop/cart/update/' + id,
        data:{id: id, qty: $(this).val()},
        success: function (data) {
          location.reload();
        },
        error: function (data) {
          console.log('Error:', data);
        }
      });
    })
  });
</script>
@endsection
@extends('shop.layout.app')

@section('content')

<!-- Main Container -->
<section class="main-container col1-layout">
  <div class="main">
    <div class="container">
      <div class="row">
        <!-- Breadcrumbs -->
        <div class="breadcrumbs">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <ul>
                  <li class="home"> <a href="{{ route('index') }}" title="Go to Home Page">Home</a> <span>/</span> </li>
                  <li class="category1599"> <a href="left_bar.html" title="">Living Room</a> </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <!-- Breadcrumbs End -->
        <div class="col-main">
          <div class="container">
            <div class="row">
              <div class="product-view">
                <div class="product-essential">
                  <form action="{{ route('product.addToCartWithQty') }}" method="post" id="product_addtocart_form">
                     {{ csrf_field() }}
                    <input name="form_key" value="6UbXroakyQlbfQzK" type="hidden">
                    <div class="product-img-box col-lg-5 col-sm-6 col-xs-12">
                      <div class="new-label new-top-left"> New </div>
                      <div class="product-image">
                        <div class="product-full">
                          <input type="hidden" name="id" value="{{ $product->id }}" id="product_id">
                          <img id="product-zoom" src="{{ URL::asset((json_decode($product->images))[0]) }}" data-zoom-image="{{ URL::asset((json_decode($product->images))[0]) }}" alt="product-image"/>
                        </div>
                        <div class="more-views">
                          <div class="slider-items-products">
                            <div id="gallery_01" class="product-flexslider hidden-buttons product-img-thumb">
                              <div class="slider-items slider-width-col4 block-content">
                                @if(isset((json_decode($product->images))[1]))
                                <div class="more-views-items"> <a href="#" data-image="{{ URL::asset((json_decode($product->images))[1]) }}" data-zoom-image="{{ URL::asset((json_decode($product->images))[1]) }}"> <img id="product-zoom"  src="{{ URL::asset((json_decode($product->images))[1]) }}" alt="product-image"/> </a></div>
                                @endif
                                @if(isset((json_decode($product->images))[2]))
                                <div class="more-views-items"> <a href="#" data-image="{{ URL::asset((json_decode($product->images))[2]) }}" data-zoom-image="{{ URL::asset((json_decode($product->images))[2]) }}"> <img id="product-zoom"  src="{{ URL::asset((json_decode($product->images))[2]) }}" alt="product-image"/> </a></div>
                                @endif
                                @if(isset((json_decode($product->images))[3]))
                                <div class="more-views-items"> <a href="#" data-image="{{ URL::asset((json_decode($product->images))[3]) }}" data-zoom-image="{{ URL::asset((json_decode($product->images))[3]) }}"> <img id="product-zoom"  src="{{ URL::asset((json_decode($product->images))[3]) }}" alt="product-image"/> </a></div>
                                @endif
                                @if(isset((json_decode($product->images))[4]))
                                <div class="more-views-items"> <a href="#" data-image="{{ URL::asset((json_decode($product->images))[4]) }}" data-zoom-image="{{ URL::asset((json_decode($product->images))[4]) }}"> <img id="product-zoom"  src="{{ URL::asset((json_decode($product->images))[4]) }}" alt="product-image"/> </a> </div>
                                @endif
                                @if(isset((json_decode($product->images))[5]))
                                <div class="more-views-items"> <a href="#" data-image="{{ URL::asset((json_decode($product->images))[5]) }}" data-zoom-image="{{ URL::asset((json_decode($product->images))[5]) }}"> <img id="product-zoom"  src="{{ URL::asset((json_decode($product->images))[5]) }}" alt="product-image" /> </a></div>
                                @endif
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- end: more-images -->
                    </div>
                    <div class="product-shop col-lg-7 col-sm-6 col-xs-12">
                      <div class="product-next-prev"> <a class="product-next" href="#"><span></span></a> <a class="product-prev" href="#"><span></span></a> </div>
                      <div class="product-name">
                        <h1>{{ $product->name }}</h1>
                      </div>
                      <div class="ratings">
                        <div class="rating-box">
                          <div style="width:60%" class="rating">
                            <div class="rating-box"> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span> </div>
                          </div>
                        </div>
                        <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#">Add Your Review</a> </p>
                      </div>
                      <div class="price-block">
                        <div class="price-box">
                          <p class="special-price"> <span class="price-label">Special Price</span> <span id="product-price-48" class="price"> $ {{ $product->sell_price }} </span> </p>
                          <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"> ${{ $product->sale_price }} </span> </p>
                          <p class="availability in-stock pull-right"><span>In Stock</span></p>
                        </div>
                      </div>
                      <div class="short-description">
                        <h2>Quick Overview</h2>
                        <p>{{ $product->short_desc }}</p>
                      </div>
                      <div class="add-to-box">
                        <div class="add-to-cart">
                          <div class="pull-left">
                            <div class="custom pull-left">
                              <button onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;" class="reduced items-count" type="button"><i class="fa fa-minus">&nbsp;</i></button>
                              <input type="text" class="input-text qty" title="Qty" value="1" maxlength="12" id="qty" name="qty">
                              <button onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" class="increase items-count" type="button"><i class="fa fa-plus">&nbsp;</i></button>
                            </div>
                          </div>
                          <button class="button btn-cart" title="Add to Cart" type="submit">Add to Cart</button>
                        </div>
                        <div class="email-addto-box">
                          <ul class="add-to-links">
                            <li><span class="separator">|</span> <a class="link-compare" href="compare.html"><span>Add to Compare</span></a></li>
                          </ul>
                          <p class="email-friend"><a href="#" class=""><span>Email to a Friend</span></a></p>
                        </div>
                      </div>
                      <div class="social">
                        <ul>
                          <li class="fb pull-left"><a href="#"></a></li>
                          <li class="tw pull-left"><a href="#" target="_blank"></a></li>
                          <li class="googleplus pull-left"><a href="#" target="_blank"></a></li>
                          <li class="rss pull-left"><a href="#" target="_blank"></a></li>
                          <li class="pintrest pull-left"><a href="#" target="_blank"></a></li>
                          <li class="linkedin pull-left"><a href="#" target="_blank"></a></li>
                          <li class="youtube pull-left"><a href="#" target="_blank"></a></li>
                        </ul>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
          <div class="container">
          <div class="product-collateral col-lg-12 col-sm-12 col-xs-12">
            <div class="add_info">
              <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
                <li class="active"><a href="#product_tabs_tags" data-toggle="tab">Features</a></li>
                <li> <a href="#product_tabs_description" data-toggle="tab"> Product Description </a> </li>
                <li> <a href="#reviews_tabs" data-toggle="tab">Reviews</a> </li>
                {{--<li> <a href="#product_tabs_custom" data-toggle="tab">Custom Tab</a> </li>--}}
                {{--<li> <a href="#product_tabs_custom1" data-toggle="tab">Custom Tab1</a> </li>--}}
              </ul>
              <div id="productTabContent" class="tab-content">
                <div class="tab-pane fade" id="product_tabs_description">
                  <div class="std">
                    <p>{!! $product->description !!}</p>
                  </div>
                </div>
                <div class="tab-pane fade  in active" id="product_tabs_tags">
                  <div class="box-collateral box-tags">
                    <div class="tags">
                      <table width="100%; font-size: 14px">
                        <tbody>
                          <tr>
                            <td style="width:10%; font-weight: bold;">Material:</td>
                            <td>{{ $product->material }}</td>
                          </tr>
                          <tr>
                            <td style="width:10%; font-weight: bold;">Color:</td>
                            <td>{{ $product->color }}</td>
                          </tr>
                          <tr>
                            <td style="width:10%; font-weight: bold;">Size:</td>
                            <td>{{ $product->size }}</td>
                          </tr>
                          <tr>
                            <td style="width:10%; font-weight: bold;">Made in:</td>
                            <td>{{ $product->made_in }}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="reviews_tabs">
                  <div class="box-collateral box-reviews" id="customer-reviews">
                    <div class="box-reviews1">
                      <div class="form-add">
                        <div class="review-form">
                          <h3>Write Your Own Review</h3>
                          <fieldset>
                            <h4>How do you rate this product? <em class="required">*</em></h4>
                            <span id="input-message-box"></span>
                            <table id="product-review-table" class="data-table">
                              <colgroup>
                                <col>
                                <col width="1">
                                <col width="1">
                                <col width="1">
                                <col width="1">
                                <col width="1">
                              </colgroup>
                              <thead>
                                <tr class="first last">
                                  <th>&nbsp;</th>
                                  <th><span class="nobr">1 *</span></th>
                                  <th><span class="nobr">2 *</span></th>
                                  <th><span class="nobr">3 *</span></th>
                                  <th><span class="nobr">4 *</span></th>
                                  <th><span class="nobr">5 *</span></th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr class="first odd">
                                  <th>Happy</th>
                                  <td class="value"><input type="radio" class="radio" value="1" id="Price_1" name="ratings"></td>
                                  <td class="value"><input type="radio" class="radio" value="2" id="Price_2" name="ratings"></td>
                                  <td class="value"><input type="radio" class="radio" value="3" id="Price_3" name="ratings"></td>
                                  <td class="value"><input type="radio" class="radio" value="4" id="Price_4" name="ratings"></td>
                                  <td class="value last"><input type="radio" class="radio" value="5" id="Price_5" name="ratings" required></td>
                                </tr>
                              </tbody>
                            </table>
                            <input type="hidden" value="" class="validate-rating" name="validate_rating">
                            <div class="review1">
                              <ul class="form-list">
                                <li>
                                  <label class="required" for="nickname_field">Nickname<em>*</em></label>
                                  <div class="input-box">
                                    <input type="text" class="input-text" id="nickname_field" name="nickname" required>
                                  </div>
                                </li>
                                <li>
                                  <label class="required" for="summary_field">Summary<em>*</em></label>
                                  <div class="input-box">
                                    <input type="text" class="input-text" id="summary_field" name="title" required>
                                  </div>
                                </li>
                              </ul>
                            </div>
                            <div class="review2">
                              <ul>
                                <li>
                                  <label class="required " for="review_field">Review<em>*</em></label>
                                  <div class="input-box">
                                    <textarea rows="3" cols="5" id="review_field" name="detail" required></textarea>
                                  </div>
                                </li>
                              </ul>
                              <div class="buttons-set">
                                <button class="button submit" title="Submit Review" type="submit"><span>Submit Review</span></button>
                              </div>
                            </div>
                          </fieldset>
                        </div>
                      </div>
                    </div>
                    <div class="box-reviews2">
                      <h3>Customer Reviews</h3>
                      <div class="box visible">
                        <ul>
                          <li>
                            <div class="review">
                              <h6><a href="#">Good Product</a></h6>
                              <small>Review by <span>John Doe </span>on 25/8/2016 </small>
                              <div class="rating-box">
                                <div class="rating" style="width:100%;"></div>
                              </div>
                              <div class="review-txt"> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</div>
                            </div>
                          </li>
                          <li class="even">
                            <div class="review">
                              <h6><a href="#/catalog/product/view/id/60/">Superb!</a></h6>
                              <small>Review by <span>John Doe</span>on 12/3/2015 </small>
                              <div class="rating-box">
                                <div class="rating" style="width:100%;"></div>
                              </div>
                              <div class="review-txt"> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book </div>
                            </div>
                          </li>
                          <li>
                            <div class="review">
                              <h6><a href="#/catalog/product/view/id/59/">Awesome Product</a></h6>
                              <small>Review by <span>John Doe</span>on 28/2/2015 </small>
                              <div class="rating-box">
                                <div class="rating" style="width:100%;"></div>
                              </div>
                              <div class="review-txt last"> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div class="clear"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="bestsell-pro">
              <div class="slider-items-products">
                <div class="bestsell-block">
                  <p class="h2 products-section-title"> <span>Related Products</span> </p>
                  <div id="bestsell-slider" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4 products-grid block-content">
                      @foreach($relatedProduct as $product)
                      <div class="item">
                        <div class="item-inner">
                          <div class="productborder">
                            <div class="item-img">
                              <div class="item-img-info"> <a class="product-image" title="Sample" href="{!! route('product.detail', ['product_id'=>$product->id, 'brand_id' => $product->brand_id ]) !!}"> <img alt="Sample" src="{{  URL::asset((json_decode($product->images))[0]) }}"></a>
                                <div class="new-label new-top-right">new</div>
                                <div class="quickview top-right"> <a class="link-quickview" href="quick_view.html"> <i class="fa fa-search" aria-hidden="true"></i></a> </div>
                                <div class="box-hover">
                                  <ul class="add-to-links">
                                    <li><a class="link-wishlist" href="wishlist.html"></a> </li>
                                    <li><a class="link-compare" href="compare.html"></a> </li>
                                    <li><a class="add-to-cart" href="shopping_cart.html"></a> </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <div class="right-block">
                              <div class="item-info">
                                <div class="info-inner">
                                  <div class="item-title"> <a title="product lapen casen" href="{!! route('product.detail', ['product_id'=>$product->id, 'brand_id' => $product->brand_id ]) !!}">{{ $product->name }}</a> </div>
                                  <div class="item-content">
                                    <p class="product-manufacturer">{{ $product->brand_name }}</p>
                                  </div>
                                  <div class="item-price">
                                    <div class="price-box"> <span class="regular-price"> <span class="price">${{ $product->sell_price }}</span> </span> </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      @endforeach
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Main Container End -->
@endsection
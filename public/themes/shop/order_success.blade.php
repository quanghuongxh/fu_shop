@section('title')
Shopping online - Buy online onn fushop.vn
@endsection

@extends('shop.layout.app')

@section('content')
<div class="container">
 <h2>Thank you! Your order has been placed.</h2>
 <h5>Your order is: #{{ $order->id }} </h5>
 <p>An email receipt including the detail's about your order has been sent to  {{ $order->email }}</p>
 <h5>This order will be shipped to:</h5>
 <p>Name:  {{ $order->name }}</p>
 <p>Phone Number:  {{ $order->phone }}</p>
 <p>Address:  {{ $order->address }}</p>
 {{-- <h5>Payment Method:  {{ $order->payment_method }}</h5> --}}
 <a href="{!! route('index') !!}" class="button btn-continue" title="Continue Shopping" type="button">
  <span>Continue Shopping</span>
</a>
</div>
@endsection
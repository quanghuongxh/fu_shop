@extends('shop.layout.app')
@section('style')
<style>
img.avatar {
  border: 1px solid #eee;
  width: 35%;
}

.only-bottom-margin {
  margin-top: 0px;
}

.activity-mini {
  padding-right: 15px;
  float: left;
}
</style>
@endsection
@section('content')
<div class="row">
  <div class="col-md-8 col-md-offset-2">
    <br>
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12 lead"><h4>My Orders</h4><hr></div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  @if(count($orders)>0)
                  @foreach($orders as $order)
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <ul class="list-group">
                        @foreach($order->cart->products as $product)
                           <li class="list-group-item"> {{ $product['product']['name'] }} | {{ $product['qty'] }} Units <span class="badge">${{ $product['price'] }}</span></li>
                        @endforeach
                      </ul>
                    </div>
                    <div class="panel-footer">
                      <strong>Order is: #{{ $order->id }} | Total Price: ${{ $order->cart->totalPrice }}</strong>
                    </div>
                  </div>
                  @endforeach
                  @else
                    <div class="row" align="center">
                  <h4>You have no orders</h4>
                      <a href="{!! route('index') !!}" class="button btn-continue" title="Continue Shopping"type="button">
                      <span>Continue Shopping</span>
                    </a>
                    </div>
                  @endif
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
<?php
 return [

     'num_items_show' => 7,


     //order status
     'order' => [
	     '0' => 'cancle',
	     '1' => 'completed',
	     '2' => 'pending',
	     '3' => 'approach',
     ],


     //default status
     'status' => [
         '1' => 'available',
         '0' => 'disable',
     ],


 ];
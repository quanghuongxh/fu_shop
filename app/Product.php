<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'products';

    protected $fillable = [
        'name', 'code', 'description', 'content', 'category_id',
        'brand_id', 'images', 'stock_id', 'quantity', 'import_price', 'sell_price', 'sale_price',
        'is_featured', 'available','color','size','material','made_in',
    ];


    public function category() {
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function brand() {
        return $this->belongsTo('App\Brand', 'brand_id');
    }

    public function stock() {
        return $this->belongsTo('App\Stock', 'stock_id');
    }

    //order
    public function orders() {
        $this->belongsToMany('App\Order','product_order', 'product_id', 'order_id')
             ->withPivot('quantity')
             ->withTimestamps();
    }


    //SUPPORT FUNCTION
    /**
     * Get the array of images product
     * @param Product $product
     */
    public function get_product_images() {
        return json_decode($this->images, true);
    }

    /**
     * @desc Get featured image of product
     * @param Product $_product
     * @return first image of array
     */
    public function featured_img() {
        $image_array = $this->get_product_images();
        return $image_array[0];
    }


}

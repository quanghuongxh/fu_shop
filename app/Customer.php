<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'users';

    protected $fillable = [
        'email',
        'first_name',
        'last_name',
        'avatar',
        'phone',
        'address',
    ];

    public function orders() {
        return $this->hasMany('App\Order');
    }

    public function reviews() {
        return $this->hasMany('App\Review');
    }

    
}

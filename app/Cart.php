<?php

namespace App;


class Cart
{
    public $products = null;
    public $totalQty = 0;
    public $totalPrice = 0;
    public $date = null;

    public function __construct($oldCart)
    {
        if ($oldCart) {
            $this->products = $oldCart->products;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;
            $this->date = $oldCart->date;
        }
    }

    public function add($product, $id)
    {
        $storeProduct = [
            'qty' => 0,
            'price' => $product->sell_price,
            'product' => $product
        ];
        if ($this->products) {
            if (array_key_exists($id, $this->products)) {
                $storeProduct = $this->products[$id];
            }

        }
        $storeProduct['qty']++;
        $storeProduct['price'] = $product->sell_price * $storeProduct['qty'];
        $this->products[$id] = $storeProduct;
        $this->totalQty++;
        $this->totalPrice += $product->sell_price;
    }

    public function addWithQty($product, $id, $qty)
    {
        $storeProduct = [
            'qty' => 0,
            'price' => $product->sell_price,
            'product' => $product
        ];
        if ($this->products) {
            if (array_key_exists($id, $this->products)) {
                $storeProduct = $this->products[$id];
            }
        }

        $storeProduct['qty'] += $qty;

        $storeProduct['price'] = $product->sell_price * $storeProduct['qty'];

        $this->products[$id] = $storeProduct;

        $this->totalQty += $qty;

        $this->totalPrice += ($product->sell_price * $qty);

    }

    public function updateQty($id, $qty)
    {
    	$this->totalQty = $this->totalQty -  $this->products[$id]['qty'] + $qty;
	    $this->totalPrice = $this->totalPrice - $this->products[$id]['price'] + ( $this->products[$id]['product']['sell_price'] * $qty);
	    $this->products[$id]['qty'] = $qty;
	    $this->products[$id]['price'] = $this->products[$id]['product']['sell_price'] * $qty;
    }

    public function removeProduct($id)
    {
        $this->totalQty -= $this->products[$id]['qty'];
        $this->totalPrice -= $this->products[$id]['price'];
        unset($this->products[$id]);
     }

    public function reduceByOne($id) {
        $this->products[$id]['qty']--;
        $this->products[$id]['price'] -= $this->products[$id]['product']['price'];
        $this->totalQty--;
        $this->totalPrice -= $this->products[$id]['product']['price'];

        if ($this->products[$id]['qty'] <= 0) {
            unset($this->products[$id]);
        }
    }
}
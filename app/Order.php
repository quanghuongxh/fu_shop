<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $table = 'orders';

    protected $fillable = [
        'total_price', 'custommer_id', 'address', 'phone', 'emails',
    ];

    public function products() {
        return $this->belongsToMany('App\Product','product_order', 'order_id', 'product_id')
             ->withPivot('quantity')
             ->withTimestamps();
    }


    public function reviews() {
        return $this->hasMany('App\Review');
    }


    public function customer() {
        return $this->belongsTo('App\Customer');
    }
}

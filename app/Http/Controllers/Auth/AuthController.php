<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\LoginRequest;
// use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Http\Controllers\Controller;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

class AuthController extends Controller
{
    /**
     * dispay login form
     * @return view login form
     */
    public function getLogin()
    {
      return view('auth.login');
    }
    
    public function getCustomerLogin(){
    	return view('auth.customer_login');
    }
    /**
     * Do login action
     * @param  LoginRequest $request request from login form
     * @return [type]                [description]
     */
    public function postLogin(LoginRequest $request)
    {
      try
      {
        $remember = (bool) $request->get('remember', false);
        if ($user = Sentinel::authenticate($request->all(), $remember)){
          if($user->inRole('admin')){
            session(['user_id' => $user->id, 'user_name' => $user->first_name . $user->last_name]);
            return redirect()->route('dashboard');
          }else{
            $err = "See you late, if you can!";
          }
        }else{
          $err = "Tên đăng nhập hoặc mật khẩu không đúng";
        }

      }

      catch (NotActivatedException $e)
      {
        $err = "Tài khoản của bạn chưa được kích hoạt";
      }

      catch (ThrottlingException $e)
      {
        $delay = $e->getDelay();
        $err = "Tài khoản của bạn bị block trong vòng {$delay} sec";
      }

      return redirect()->back()
      ->with('nofi_fail','loi');

    }
    // customer login
    public function postCustomerLogin(LoginRequest $request)
    {
      try
      {
        $remember = (bool) $request->get('remember', false);
        $user = Sentinel::authenticate($request->all());
        if ($user = Sentinel::authenticate($request->all(), $remember))
          {
            session(['user_id' => $user->id, 'user_name' => $user->first_name . $user->last_name]);

            return redirect()->route('index');
          }
          else
          {
            $err = "Tên đăng nhập hoặc mật khẩu không đúng";
          }

        }

        catch (NotActivatedException $e)
        {
          $err = "Tài khoản của bạn chưa được kích hoạt";
        }

        catch (ThrottlingException $e)
        {
          $delay = $e->getDelay();
          $err = "Tài khoản của bạn bị block trong vòng {$delay} sec";
        }

        return redirect()->back()
        ->with('nofi_fail','loi');

      }

      public function logout()
      {
        Sentinel::logout();
        Session::flush();
        return redirect()->back();
      }

    }

<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use App\Http\Requests\RegisterRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    public function __construct()
    {

    }

    public function getRegister()
    {
        return view('auth.register');
    }


    public function getCustomerRegister(){
        return view('auth.customer_register');
    }


    public function postRegister(RegisterRequest $request)
    {

        $credentials = [
            'email'    => $request->email,
            'password' => $request->password,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name
        ];
        $user = Sentinel::register($credentials);

        $user_id = $user->id;

        $activation = Activation::create($user);

        $confirmation_code = $activation->code;
        
        //ranh viet try cache

        Mail::send('email.verify_account', compact('confirmation_code','user_id') , function($message) use ($confirmation_code) {
            $message->to(Input::get('email'), null)
                    ->subject('Verify your account on Furniture Shop');
        });

       

        return redirect()->route('customer.register.success')->with('email', $request->email );
    }

    public function active(Request $request,$user_id, $confirmationCode)
    {
        $user = Sentinel::findById($user_id);

        if (Activation::complete($user, $confirmationCode)) {
            $role = Sentinel::findRoleBySlug('customer');
            $role->users()->attach($user);
            return redirect()->route('customer.login');
        }
        else {
            return redirect()->route('customer.register')->with([
                'msg_success' => false,
                'msg' => 'Sorry, we cannot activate your email, please contact to admin',
            ]);
        }
    }

}

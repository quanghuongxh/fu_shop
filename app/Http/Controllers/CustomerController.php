<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use App\Customer;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    //Show profile
	public function getProfile(){
			//Get current user

			$current_user_id = Sentinel::getUser()->id;
			//Get all order of this user
			$user = Customer::findOrFail($current_user_id);
			return view('shop.profile',['user' =>$user]); 
		}

		//Change profile

		public function postProfile(ProfileRequest $request){
			$user = User::find($request->id);
				$user->password = Hash::make($request->newPassword);
        $user->first_name = $request->firstName;
        $user->last_name = $request->lastName;
        $user->phone = $request->phone;
        $user->address = $request->address;
        try {
            $user->save();
            return redirect()->route('cus.profile')->with('noti_success','Update your information succesful!');
        }
        catch (Exception $ex) {
            return redirect()->back()->with('noti_fail','Update fail: ' . $ex->getMessage());
        }
		}

		//Change password

		//Show orders
		public function getOrder(){
			//Get current user
			$current_user_id = Sentinel::getUser()->id;
			//Get all order of this user
			$orders = DB::table('orders')->where('customer_id','=',$current_user_id)->orderBy('created_at','desc')->get();
        $orders->transform(function($order, $key) {
            $order->cart = unserialize($order->cart);
            return $order;
        });
			return view('shop.order_history',['orders' => $orders]); 
		}
		//Show my reviews

		public function getReview(){
			//Get current user
			$current_user_id = Sentinel::getUser()->id;
			//Get all order of this user
			$orders = DB::table('reviews')->where('customer_id','current_user_id')->orderBy('created_at','desc')->get();
			return view('shop.reviews',['orders' => $orders]); 
		}

		//Insert Review
		public function postReview(ReviewRequest $request){
			//handel ajax
			$review = new Review;
  
		}

}

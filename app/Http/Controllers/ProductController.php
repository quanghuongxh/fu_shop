<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Support\Facades\DB;
use Cartalyst\Sentinel\Native\Facades\Sentinel;


class ProductController extends Controller
{

    public function index(){
        //Top newest products
        $newestProduct = Product::orderBy('created_at','desc')->take(3)->get();

        //Top seller
        $bestSellerProducts = Product::orderBy('num_sell','desc')->take(3)->get();

        //Top rate
        $rateProducts = Product::orderBy('rate','desc')->take(3)->get();

        $popularProduct =  DB::table('products as p')
            ->select('p.*', 'm.name as brand_name')
            ->leftJoin('brands as m', 'm.id', '=', 'p.brand_id')
            ->orderBy('p.created_at','asc')
            ->take(4)
            ->get();
        return view('shop.index', compact('newestProduct','popularProduct', 'bestSellerProducts', 'rateProducts'));
    }

//    Show product detail
    public function showProductDetail($product_id, $brand_id){

        $product = Product::findOrFail($product_id);

        // Show related product
        $relatedProduct =  DB::table('products')
            ->leftJoin('brands', 'brands.id', '=', 'brand_id')
            ->select('products.*', 'brands.name as brand_name' )
            ->where('brand_id', '=', $brand_id)->take(7)->get();

        return view('shop.product_detail', compact('product','relatedProduct'));
    }

    // Show Quick View Product

    public function showQuickViewProduct($product_id){
        $product = Product::findOrFail($product_id);

        return view('shop.quick_view', compact('product'));
    }

}
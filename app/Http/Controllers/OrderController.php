<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Cart;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Session;
use Auth;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use App\Http\Requests\CheckoutRequest;
use Illuminate\Routing\Redirector;

use Stripe\Stripe;
use Stripe\Charge;


class OrderController extends Controller
{
    // Add to cart
	public function getAddToCart(Request $request, $id){
		$product = Product::findOrFail($id);
		$oldCart = Session::has('cart') ? Session::get('cart') : null;
		$cart = new Cart($oldCart);
		$cart->add($product, $product->id);
		$request->session()->put('cart', $cart);
		return redirect()->back();
	}

	 // Add to cart with qty
	public function postAddToCartWithQty(Request $request){
		$product = Product::findOrFail($request->id);
		$oldCart = Session::has('cart') ? Session::get('cart') : null;
		$cart = new Cart($oldCart);
		$cart->addWithQty($product,$request->id, $request->qty);
		$request->session()->put('cart', $cart);	
		return redirect()->route('product.cart', ['products' => $cart->products, 'totalPrice' => $cart->totalPrice]);
	}

//    Get cart
	public function getCart(){
		if(!Session::has('cart')){
			return view('shop.cart', ['products' => null]);
		}
		$oldCart = Session::get('cart');
		$cart = new Cart($oldCart);
		return view('shop.cart', ['products' => $cart->products, 'totalPrice' => $cart->totalPrice]);
	}

    //Get checkout
	public function getCheckout(){
		if (!Session::has('cart')){
			return view('shop.cart');
		}
		$oldCart = Session::get('cart');
		$cart = new Cart($oldCart);
		$total = $cart->totalPrice;
		return view('shop.checkout',['total' =>$total]);
	}

    //    Empty cart
	public function destroyCart(){
		Session::forget('cart');
		return Response::json();
	}
	public function getRemoveProduct($id) {
		$oldCart = Session::has('cart') ? Session::get('cart') : null;
		$cart = new Cart($oldCart);
		$cart->removeProduct($id);
		if (count($cart->products) > 0){
			Session::flush('$cart');
			Session::put('cart',$cart);
		}else{
			Session::forget('cart');
		}
		return redirect()->route('product.cart');
	}
	public function updateCart(Request $request) {
		$oldCart = Session::has('cart') ? Session::get('cart') : null;
		$cart = new Cart($oldCart);
		$cart->updateQty($request->id, $request->qty);
		Session::put('cart', $cart);
		$data = array([
			'id' => $cart->products[$request->id]['product']['id'],
			'price' => $cart->products[$request->id]['price'],
			'totalPrice' => $cart->totalPrice,
		]);
		return Response::json($data);
	}
//checkout
	public function postCheckout(CheckoutRequest $request){
		if(!Session::has('cart')){
			return redirect('product.cart');
		}
		$oldCart = Session::get('cart');
		$cart = new Cart($oldCart);

		Stripe::setApiKey('sk_test_c5INFtw9yzkS2X4OyVcPtTel');

		try {
			$charge = Charge::create(array(
				"amount" => $cart->totalPrice * 100,
				"currency" => "usd",
				"source" => $request->input('stripeToken'), // obtained with Stripe.js
				"description" => $request->input('name')
			));

			$order = new Order();

			$order->cart = serialize($cart);

			$order->name = $request->input('name');
			$order->phone = $request->input('phone');
			$order->email = $request->input('email');
			$order->address = $request->input('address');
			$order->payment_id = $charge->id;
			$order->total_price = $cart->totalPrice;
			$order->customer_id = Sentinel::getUser()->id;
			$order->payment_method = $request->input('payment-method');
			$order->status = 1;
			$order->save();

			foreach ($cart->products as $product_cart){ //Insert into product_order
				DB::table('product_order')->insert([
					'product_id' =>  $product_cart['product']->id,
					'order_id' =>  $order->id,
					'quantity' => $product_cart['qty'],
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now()
				]);
				
				// decreasing qty product after bought
				$product = Product::find($product_cart['product']->id);
				$product->quantity -= $product_cart['qty'];
				$product->save();
			}
		} catch (\Exception $e) {
			return redirect(route('checkout'))->with('error', $e->getMessage());
		}
		Session::forget('cart');
		// return view mua thang thanh cong
		// return redirect()->route('order.success');
		return view('shop.order_success', compact('order'));

		// return redirect()->route('order.success',['order' => $order]);
	} 
}
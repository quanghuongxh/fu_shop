<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Brand;

class BrandController extends Controller
{
    public function __construct() 
    {
        
    }

    public function index()
    {
    	$brands = Brand::where('id','!=',1)
    					->orderBy('created_at', 'desc')
    					->paginate( config('shop.num_items_show') );
    	return view('admin.brand.list')->with(['list_brand' => $brands]);
    }

    public function detail($id) {
        $brand = Brand::find($id);
        if (! $brand) {
            return redirect()->route('brand.list')->with('noti_fail','Danh mục không tồn tại');
        }
        
  
        return view('admin.brand.edit')->with(['brand' => $brand]);
    }

    public function postEdit(Request $request) {

        $brand = Brand::find($request->id);
        if (!$brand) {
            return redirect()->back()->with('noti_fail', 'Record not found!');
        }
        $brand->name = $request->name;
        $brand->image = $request->image;
    
        $brand->available = $request->available ? 1 : 0;

        try {
            $brand->save();
            return redirect()->route('brand.create')->with('noti_success','Update brand succesful!');
        }
        catch (Exception $ex) {
            return redirect()->back()->with('noti_fail','Update fail: ' . $ex->getMessage());
        }

    }



    public function getCreate() {
        return view('admin.brand.create');
    }

    public function postCreate(Request $request) {
        $brand = Brand::create($request->all());
        if ($request->ajax()) {
            return response()->json(['brand' => $brand], 200);
        }
        return redirect()->route('brand.create')->with('noti_success', 'Create brand succesfull!');
    }


    public function delete(Request $request, $id) {
        $brand = Brand::find($id);
        try
        {
            $brand->delete();
            return redirect()->route('brand.list')->with('noti_success', 'Xóa thành công');
        }
        catch ( Illuminate\Database\QueryException $e )
        {
            return redirect()->route('brand.list')->with('noti_fail', 'Xóa không thành công');
        }
    }
}

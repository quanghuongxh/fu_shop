<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\StoreCustomer;
use App\Http\Requests\EditCustomer;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Redirect;
use App\Customer;

use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    //
    public function __construct() {

    }

    public function index() {
        $customer_role = Sentinel::findRoleBySlug('customer');
        $customers = $customer_role->users()->paginate(10);
    	
    	return view('admin.customer.list')->with(['list_customer' => $customers]);
    }


    /*****************************************************************************
     * Show detail of customer
     * @param  [type] $id [description]
     * @return [type]     [description]
     ****************************************************************************/
    public function detail($id) {
    	$customer = Customer::find($id);
    	if ($customer) {
    		return view('admin.customer.edit')->with(['customer' => $customer]);
    	} 
    	else {
    		return Redirect::route('customer.list')->with('noti_fail', 'No customer found!');
    	}
    }
    /**
     * Store customer after edit
     * @return [type] [description]
     */
    public function postEdit(EditCustomer $request) {
        $customer = Customer::find($request->id);
        
        if (! $customer) {
            return Redirect::back()->with('noti_fail', 'Không tìm thấy Khách hàng');
        }

        $customer->fill($request->all());
       
        try
        {
            $customer->save();
            return Redirect::route('customer.list')->with('noti_success','Sửa khách hàng thành công');
        }
        catch (Illuminate\Database\QueryException $e)
        {
            return Redirect::back()->with('noti_fail', 'Sửa khách hàng không thành công ' . 'Error: ' . $e->getMessage());
        }
    }

    /**
     * [getCreate description]
     * @return [type] [description]
     */
    public function getCreate() {
    	return view('admin.customer.create');
    }

    public function postCreate(StoreCustomer $request) {
    	$customer = new Customer;
    	$customer->email = $request->email;
    	$customer->password = bcrypt($request->password);
    	$customer->first_name = $request->first_name;
    	$customer->last_name = $request->last_name;
    	$customer->phone = $request->phone;
    	$customer->address = $request->address;
    	// $customer->company = $request->company;
    	// $customer->note = $request->note;
    	$customer->is_customer = true;


    	try
        {
            $customer->save();
            // update role customer
			$role = Sentinel::findRoleByName('Customer');
			$role->users()->attach($customer);

            return Redirect::route('customer.list')->with('noti_success','Thêm khách hàng thành công');
        }
        catch (Illuminate\Database\QueryException $e)
        {
            return Redirect::back()->with('noti_fail', 'Thêm khách hàng không thành công ' . 'Error: ' . $e->getMessage());
        }
    }


    public function delete($id) {
    	$customer = Customer::find($id);
        try
        {
            $customer->delete();
            return Redirect::route('customer.list')->with('noti_success', 'Xóa thành công');
        }
        catch ( Illuminate\Database\QueryException $e )
        {
            return Redirect::route('customer.list')->with('noti_fail', 'Xóa không thành công');
        }
    }

}

<?php

namespace App\Http\Controllers\Admin;

use Cartalyst\Sentinel\Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Middleware\CheckUser;

class DashboardContrller extends Controller
{
    public function __construct() {

    }

    public function index() {
        $data['num_new_order'] = get_num_orders();
        $data['num_product'] = get_num_products();
        $data['num_customer'] = get_num_customers();

        return view('admin.dashboard')->with($data);
    }
}

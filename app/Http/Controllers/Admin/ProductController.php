<?php

namespace App\Http\Controllers\Admin;

use App\Http\Middleware\CheckUser;
use App\Http\Requests\EditProduct;
use Illuminate\Http\Request;
use App\Http\Requests\StoreProduct;
use App\Product;
use App\Category;
use App\Brand;
use App\Stock;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use PhpParser\Node\Stmt\If_;

class ProductController extends Controller
{
    /**
     * ProductController constructor.
     */
    public function __construct() {
        //$this->middleware('checkuser');
    }

    /**
     * @return view of list product
     */
    public function index() {
        $products = Product::with('category')->orderBy('created_at', 'desc')->paginate( config('shop.num_items_show') );
        return view('admin.product.list')->with(['list_product' => $products]);
    }

    /**
     * @param $id id of product
     * return view of product detail
     */
    public function detail($id) {
        $product = Product::find($id);
        if ($product) {
            $categories = Category::all('id', 'name');
            $brands = Brand::all('id', 'name');
            $stocks = Stock::all('id', 'name');

            return view('admin.product.edit')
                   ->with([
                       'list_category' => $categories,
                       'list_brand' => $brands,
                       'list_stock' => $stocks,
                       'product' => $product
                   ]);
        }
        else {
            return back()->with('noti_fail', 'No id found');
        }
    }

    public function postEdit(EditProduct $request) {
        $product = Product::find($request->id);
        if (! $product) {
            return redirect::back()->with('noti_fail', 'Không tìm thấy sản phẩm');
        }
        $product->fill($request->all());
    
        $product->is_featured = true ? $request->is_feaured == 'on' : false;
        $product->available = true ? $request->available == 'on' : false;
        try
        {
            $product->save();
            return Redirect::route('product.list')->with('noti_success','Sửa sản phẩm thành công');
        }
        catch (Illuminate\Database\QueryException $e)
        {
            return Redirect::back()->with('noti_fail', 'Sửa dữ liệu không thành công ' . 'Error: ' . $e->getMessage());
        }


    }

    /**
     *  show form to create product
     */
    public function getCreate() {
        $categories = Category::all('id', 'name');
        $brands = Brand::all('id', 'name');
        $stocks = Stock::all('id', 'name');

        return view('admin.product.create')
            ->with([
                'list_category' => $categories,
                'list_brand' => $brands,
                'list_stock' => $stocks,
            ]);
    }

    /**
     * @param StoreProduct $request
     * @return mixed
     */
    public function postCreate(StoreProduct $request) {
        $product  = new Product;
        $product->name = $request->name;
        $product->code = $request->code;
        $product->description = $request->description;
        $product->content = $request->content;
        $product->category_id = $request->category_id;
        $product->images = $request->images;
        $product->sell_price = $request->sell_price;
        $product->sale_price = $request->sale_price;
        $product->quantity = $request->quantity;
        $product->stock_id = $request->stock_id;
        $product->available = $request->available ? $request->available : 0;
        $product->is_featured = $request->is_featured ? $request->is_featured : 0;
        //addition
        $product->color = $request->color;
        $product->size = $request->size;
        $product->material = $request->material;
        $product->made_in = $request->made_in;
        try
        {
            $product->save();
            return Redirect::route('product.list')->with('noti_success','Thêm sản phẩm thành công');
        }
        catch (Illuminate\Database\QueryException $e)
        {
            return Redirect::back()->with('noti_fail', 'Thêm dữ liệu không thành công ' . 'Error: ' . $e->getMessage());
        }
    }

    /**
    *
    **/
    public function delete($id) {
        $product = Product::find($id);
        $product_name = $product->name;
        try
        {
            $product->delete();
            return Redirect::route('product.list')->with('noti_success', 'Xóa sản phẩm '.$product_name.' thành công');
        }
        catch ( Illuminate\Database\QueryException $e )
        {
            return Redirect::back()->with('noti_fail', 'Xóa sản phẩm không thành công');
        }
    }

}

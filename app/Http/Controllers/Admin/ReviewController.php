<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Stock;

class ReviewController extends Controller
{
    //
    public function __construct() {

    }

    public function index() {

    	return view('admin.review.list');
    }
}

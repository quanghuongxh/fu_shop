<?php

namespace App\Http\Controllers\Admin;

use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use App\Http\Requests\EditCategory;
use App\Http\Requests\StoreCategory;
use App\Http\Controllers\Controller;

use App\Category;
use Mockery\Exception;

class CategoryController extends Controller
{
    //
    public function __construct() {

    }

    public function index() {
    	$categories = Category::where('id','!=',1)
    							->orderBy('created_at', 'desc')
    							->paginate( config('shop.num_items_show') );
    	return view('admin.category.list')->with(['list_category' => $categories]); 
    }

    public function detail($id) {
        $category = Category::find($id);
        if (! $category) {
            return redirect()->route('category.list')->with('noti_fail','Danh mục không tồn tại');
        }
        $list_category = Category::where('available',1)
                                    ->orderBy('name', 'desc')
                                    ->get();
        $parent_category = Category::find($category->parent_id);
        return view('admin.category.edit')->with(['category' => $category,
                                                        'parent_category' => $parent_category,
                                                        'list_categories' => $list_category,
                                                    ]);
    }

    public function postEdit(EditCategory $request) {
        $category = Category::find($request->id);
        if (!$category) {
            return redirect()->back()->with('noti_fail', 'Không tìm thấy danh mục');
        }
        $category->name = $request->name;
        $category->description = $request->description;
        $category->parent_id = $request->parent_id;
        $category->available = $request->available ? $request->available : 0;

        try {
            $category->save();
            return redirect()->route('category.create')->with('noti_success','Cập nhật danh mục thành công');
        }
        catch (Exception $ex) {
            return redirect()->back()->with('noti_fail','Cập nhật loi: ' . $ex->getMessage());
        }

    }



    public function getCreate() {
        $all_categories = Category::where('available',1)
                                    ->orderBy('name','desc')
                                    ->get();

        $list_categories = Category::where('id','!=',1)
                                    ->orderBy('id','desc')
                                    ->paginate( config('shop.num_items_show') );
        return view('admin.category.create')->with(['all_category' => $all_categories, 'list_categories' => $list_categories]);

    }

    public function postCreate(StoreCategory $request) {
        $category = Category::create($request->all());
        if ($request->ajax()) {
            return response()->json(['category' => $category], 200);
        }
        return redirect()->route('category.create')->with('noti_success', 'Tạo danh mục thành công');
    }


    public function delete(Request $request, $id) {
        $category = Category::find($id);
        try
        {
            $category->delete();
            return redirect()->route('category.create')->with('noti_success', 'Xóa thành công');
        }
        catch ( Illuminate\Database\QueryException $e )
        {
            return redirect()->route('customer.create')->with('noti_fail', 'Xóa không thành công');
        }
    }



}

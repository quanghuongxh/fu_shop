<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Stock;

class StockController extends Controller
{
    //
    public function __construct() {
    	
    }

    public function index() {
    	$stocks = Stock::where('id','!=',1)
    					->orderBy('created_at', 'desc')
    					->paginate( config('shop.num_items_show') );
    	return view('admin.stock.list')->with(['list_stock' => $stocks]);
    }	

    public function getCreate() {
    	return view('admin.stock.create');
    }

    public function postCreate(Request $request) {
    	$stock = Stock::create($request->all());
        if ($request->ajax()) {
            return response()->json(['brand' => $stock], 200);
        }
        return redirect()->route('stock.list')->with('noti_success', 'Create stock succesfull!');
    }

    public function detail(Request $request, $id) {
    	$stock = Stock::find($id);
        if (! $stock) {
            return redirect()->route('stock.list')->with('noti_fail','Stock is not exist!');
        }
        return view('admin.stock.edit')->with(['stock' => $stock]);
    }

    public function postEdit(Request $request) {
    	$stock = new Stock;
    	$stock->fill($request->all());

    	$stock->save();
    	return redirect()->route('stock.list')->with('noti_success', 'Edit stock successful');

    } 

    public function delete(Request $request, $id) {
        $stock = Stock::find($id);
        try
        {
            $stock->delete();
            return redirect()->route('stock.list')->with('noti_success', 'Xóa thành công');
        }
        catch ( Illuminate\Database\QueryException $e )
        {
            return redirect()->route('stock.list')->with('noti_fail', 'Xóa không thành công');
        }
    }

}

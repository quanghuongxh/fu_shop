<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;

class OrderController extends Controller
{
    //
    public function index()
    {
        $orders = Order::with('customer')->paginate(config('app.num_items_show'));
        return view('admin.order.list')->with(['orders' => $orders]);
    }

    public function detail($id)
    {
        $order = Order::find($id)->with(['customer','products'])->first();

        return view('admin.order.edit')->with(['order' => $order]);
    }

    public function changeStatus(Request $request)
    {
        $order = Order::find($request->id);
        if (! $order) {
            return redirect()->back()->with('noti_fail', 'No order found!');
        }
        $order->status = $request->status;
        
        $order->save();

        return redirect()->route('order.list')->with('noti_success','Change status successful!');
    }

    public function destroy()
    {
        $order = Order::find($id);
        $order->delete();

        return redirect()->route('order.list')->with('noti_success','Deleted order successful!');

    }
}

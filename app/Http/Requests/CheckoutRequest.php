<?php

namespace App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class CheckoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if($request->input('payment-method') == 'credit-card' ){
            return [
                'name' => 'required|max:120|min:5',
                'phone' => 'required|numeric',
                'email' => 'required|email',
                'address' => 'required',
            ];    
        }else{
            return [
                'name' => 'required|max:120|min:5',
                'phone' => 'required|numeric',
                'email' => 'required|email',
                'address' => 'required',
            ]; 
        }
        
    }
}

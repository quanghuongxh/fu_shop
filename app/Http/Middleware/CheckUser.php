<?php

namespace App\Http\Middleware;

use Closure;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Database\Capsule\Manager as Capsule;

class CheckUser
{
    /**
     * Handle an incoming request to page auth
     * if fail redirect to login form for user login
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Sentinel::check()){
            return $next($request);
        }
        return redirect()->route('customer.login')->with('noti_fail', 'Permission deny');;
    }
}

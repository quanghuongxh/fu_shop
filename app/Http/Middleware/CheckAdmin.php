<?php

namespace App\Http\Middleware;

use Closure;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Database\Capsule\Manager as Capsule;

class CheckAdmin
{
    /**
     * Handle an incoming request page required role admin
     * If 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Sentinel::check()) {
            if (Sentinel::getUser()->inRole('admin'))
            return $next($request);
        }
        
        return redirect()->route('user.login')->with('noti_fail', 'Permission deny');
    }
}

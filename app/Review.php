<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'reviews';

    protected $fillable =[
        'order_id',
        'customer_id',
        'content',
        'star',
    ];

    public function customer() {
        return $this->belongsTo('App\Customer', 'customer_id');
    }

    public function order() {
        return $this->belongsTo('App\Order', 'order_id');
    }
}

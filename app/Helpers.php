<?php
    
    use Illuminate\Support\Facades\DB;
    use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

    function get_num_orders() {
       return App\Order::where('status','=',1)->count();
   }

    function get_num_products() {
       return App\Product::where('available', '=',1)->count();

    }

    function get_num_customers() {
        return App\Customer::where('is_customer', '=',1)->count();
    }

    function get_featured_image($feature_image) {
        $upload_dir = base_path('uploads');
        return $upload_dir . '/' . $feature_image;

    }
    function get_categories($limit = 0) {
        if ($limit > 0)
          return DB::table('categories')
                              ->where([
                                  ['id', '!=', 1],
                                  ['available', '=', 1],
                                ])
                              ->take($limit)  
                              ->get();
        return DB::table('categories')->where([
                                  ['id', '!=', 1],
                                  ['available', '=', 1],
                                ])->get();
    }


    function get_order_status($status = 0)
    {
        if ($status == 0) return config('shop.order.0');
        if ($status == 1) return config('shop.order.1');
        if ($status == 2) return config('shop.order.2');
        if ($status == 3) return config('shop.order.3');
    }
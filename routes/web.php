<?php
/**
 * All admin route here
 * prefix  admin mean url has pattern /admin/
 * namespace of controller Admin mean put all Admin controller to folder Admin
 * middleware ll apply in contructor of Controller
 */

Route::namespace('Admin')->group(function () {
	// Route::group(['prefix' => 'admin', 'middleware' => ['shop.admin']], function () {
	Route::group(['prefix' => 'admin'], function () {

		//dashboard
		Route::get('/', function () {
			return view('admin.dashboard');
		})->name('dashboard');
		/**
		 *  for order area
		 */
		Route::get('/order', 'OrderController@index')->name('order.list');
		Route::get('/order/{id}', 'OrderController@detail')->name('order.view');
		Route::post('/order/change-status', 'OrderController@changeStatus')->name('order.change-status');
		Route::delete('/order/delete/{id}', 'OrderController@destroy')->name('order.delete');

		/**
		 * for product area
		 */
		Route::get('/product', 'ProductController@index')->name('product.list');
		Route::get('/product/{id}', 'ProductController@detail')->where(['id' => '[0-9]+'])->name('product.view');
		Route::post('/product/edit', 'ProductController@postEdit')->name('product.edit');
		Route::get('/product/create', 'ProductController@getCreate')->name('product.create');
		Route::post('/product/store', 'ProductController@postCreate')->name('product.store');
		Route::delete('/product/delete/{id}', 'ProductController@delete')->name('product.delete');

		/**
		 * for category area
		 */
		Route::get('/cat', 'CategoryController@index')->name('category.list');
		Route::get('/cat/create', 'CategoryController@getCreate')->name('category.create');
		Route::post('/cat/store', 'CategoryController@postCreate')->name('category.store');
		Route::get('/cat/{id}', 'CategoryController@detail')->where(['id' => '[0-9]+'])->name('category.view');
		Route::post('/cat/edit', 'CategoryController@postEdit')->name('category.edit');
		Route::delete('/cat/delete/{id}', 'CategoryController@delete')->name('category.delete');

		/**
		 * for brand area
		 */
		Route::get('/brand', 'BrandController@index')->name('brand.list');
		Route::get('/brand/create', 'BrandController@getCreate')->name('brand.create');
		Route::post('/brand/create', 'BrandController@postCreate')->name('brand.store');
		Route::get('/brand/{id}', 'BrandController@detail')->where(['id' => '[0-9]+'])->name('brand.view');
		Route::post('/brand/edit', 'BrandController@postEdit')->name('brand.edit');
		Route::delete('/brand/delete/{id}', 'BrandController@delete')->name('brand.delete');
		/**
		 * for customer area
		 */
		Route::get('/customer', 'CustomerController@index')->name('customer.list');
		Route::get('/customer/create', 'CustomerController@getCreate')->name('customer.create');
		Route::post('/customer/store', 'CustomerController@postCreate')->name('customer.store');
		Route::get('/customer/{id}', 'CustomerController@detail')->where(['id' => '[0-9]+'])->name('customer.view');
		Route::post('/customer/edit', 'CustomerController@postEdit')->name('customer.edit');
		Route::delete('/customer/delete/{id}', 'CustomerController@delete')->name('customer.delete');
		/**
		 * for stock area
		 */
		Route::get('/stock', 'StockController@index')->name('stock.list');
		Route::get('/stock/create', 'StockController@getCreate')->name('stock.create');
		Route::post('/stock/store', 'StockController@postCreate')->name('stock.store');
		Route::get('/stock/{id}', 'StockController@detail')->where(['id' => '[0-9]+'])->name('stock.view');
		Route::post('/stock/edit', 'StockController@postEdit')->name('stock.edit');
		Route::delete('/stock/delete/{id}', 'StockController@delete')->name('stock.delete');

	});
});

/**********************************************************************
 * Auth
 */
Route::namespace('Auth')->group(function () {
	Route::group(['middleware' => ['check.user']], function () {
		Route::get('/logout', 'AuthController@logout')->name('user.logout');
		Route::post('/logout', 'AuthController@logout')->name('user.logout');

		Route::get('/orders', 'AuthController@getorder')->name('customer.order');
		Route::post('/orders', 'AuthController@postorder')->name('customer.order');

		Route::post('/profile', 'AuthController@getProfile')->name('profile');
		Route::post('/profile', 'AuthController@postProfile')->name('profile');
	});

	Route::get('/admin/login', 'AuthController@getLogin')->name('user.login');
	Route::post('/admin/login', 'AuthController@postLogin')->name('user.login');

	Route::get('/login', 'AuthController@getCustomerLogin')->name('customer.login'); //for customer login
	Route::post('/login', 'AuthController@postCustomerLogin')->name('customer.login'); //for customer login

	Route::post('/register', 'RegisterController@postRegister')->name('customer.register');
	Route::get('/register', 'RegisterController@getCustomerRegister')->name('customer.register'); //for customer register
	Route::get('register/verify/{user_id}/{confirmationCode}', 'RegisterController@active')->name('customer.activate');

	Route::get('register/success', function()  {
		return view('auth.success');
	})->name('customer.register.success');
});


/***********************************************************************
 * All front site route here
 * namespace  of controller is Shop mean all Controller is in folder Shop
 **********************************************************************/
 Route::get('/', function () {
    return redirect()->route('index');
 });

Route::group(['prefix' => 'shop', 'middleware' => 'web'], function () {
	
	Route::get('/', 'ProductController@index')->name('index'); //trang chu

	Route::get('/product-detail/{product_id}/{brand_id}', 'ProductController@showProductDetail')->name('product.detail');

	Route::get('/quick-view/{product_id}', ['uses' => 'ProductController@showQuickViewProduct', 'as' => 'quick.view']);
	
	Route::get('/add-to-cart/{id}','OrderController@getAddToCart')->name('product.addToCart');
	Route::post('/add-to-cart-qty','OrderController@postAddToCartWithQty')->name('product.addToCartWithQty');

	Route::get('/cart', 'OrderController@getCart')->name('product.cart');
	Route::get('/cart/remove/{id}', 'OrderController@getRemoveProduct')->name('cart.removeItem');
	Route::post('/cart/update/{id}', 'OrderController@updateCart')->name('cart.update');
	Route::get('cart/destroy', 'OrderController@destroyCart')->name('cart.destroy');
	
	Route::group(['middleware' => ['check.user']], function () {
		Route::get('/checkout', 'OrderController@getCheckout')->name('checkout');
		Route::post('/checkout', 'OrderController@postCheckout')->name('checkout');

		Route::get('/my-orders', 'CustomerController@getOrder')->name('cus.order');
		Route::get('/my-reviews', 'CustomerController@getReview')->name('cus.review');
		Route::get('/profile', 'CustomerController@getProfile')->name('cus.profile');
		Route::post('/profile', 'CustomerController@postProfile')->name('cus.profile');
		Route::get('/change-password', 'CustomerController@getChangePassword')->name('cus.change-password');
		Route::post('/change-password', 'CustomerController@postChangePassword')->name('cus.change-password');
	});

	Route::get('/order/success/{id}', function () {
			return view('shop.order_success');
		})->name('order.success');
});

/**
 * All test route
 */
Route::prefix('test')->group(function () {
	Route::get('/helper', function () {
		test_helper();
	});
});